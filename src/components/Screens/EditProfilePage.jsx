/**
 * Author : Aymen FEZAI
 */
import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Route, Link, withRouter} from "react-router-dom";

import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import '../../sass/layout/_profilecss.scss'
import '../../images/defaultAvatar.png'

import Switch from "react-switch";
import Dropzone from 'react-dropzone';
import FontAwesome from 'react-fontawesome';
import Loader from 'react-loader';

//For popup messages
import toastr from 'toastr'
import 'toastr/build/toastr.min.css'
import {FormattedMessage} from 'react-intl';
toastr.options = {
    positionClass: 'toast-top-right',
    "closeButton": true,
    hideDuration: 300,
    timeOut: 3000
}
//End of toastr

class EditProfilePage extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            account: {},

            name: "",
            email: "",
            gender: "",
            profilepic: "",
            country: "",
            phone: "",

            loadedProfilePic: true,
            refresh: false
        };

        this.updateProfile = this
            .updateProfile
            .bind(this);
        this.cancelupdate = this
            .cancelupdate
            .bind(this);

    }

    componentWillMount() {
        window.scroll({top: 0, left: 0, behavior: 'smooth'});
        this.setState({
            account: JSON.parse(localStorage.getItem('account'))
        })

        console.log("LocalStorage in WillMount -------")
        console.log(JSON.parse(localStorage.getItem('account')))

        let options = {}

        options.method = 'GET'

        options.headers = {
            'x-access-token': JSON
                .parse(localStorage.getItem('account'))
                .token
        }

        fetch("http://35.204.178.205:4123/users/detail/" + JSON.parse(localStorage.getItem('account')).id, options).then((res) => res.json()).then((res) => {
            if (res != null) {
                this.setState({
                    name: res.hits.hits[0]._source.name,
                    email: res.hits.hits[0]._source.email,
                    gender: res.hits.hits[0]._source.gender,
                    country: res.hits.hits[0]._source.country,
                    phone: res.hits.hits[0]._source.phone,
                    profilepic: res.hits.hits[0]._source.profilepic
                })
            }
        }).catch(err => console.error(err))

    }

    handleChange = name => event => {
        this.setState({[name]: event.target.value});
    }

    onDropProfilePic(profilePic) {
        this.setState({loadedProfilePic: false});

        let options = {};

        var data = new FormData();

        data.append('file', profilePic[0]);
        options.body = data;

        options.headers = {
            'x-access-token': this.state.account.token
        };

        options.method = 'POST';

        fetch("http://35.204.178.205:4123/users/uploadProfilePic", options).then((res) => res.json()).then((res) => {
            if (res != null) {
                this.setState({profilepic: res, loadedProfilePic: true});

                console.log("-----------upload profile pic res")
                console.log(res)
            }
        }).catch(err => console.error(err))

    }

    cancelupdate(event)
    {
        event.preventDefault();
        this
            .props
            .history
            .push('/');
    }

    updateProfile(event) {
        event.preventDefault();
        this.setState({refresh: false});

        let options = {}

        options.method = 'POST'

        options.headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'x-access-token': this.state.account.token
        }

        options.body = JSON.stringify({
            id: this.state.account.id,

            name: this.state.name,
            email: this.state.email,
            gender: this.state.gender,
            country: this.state.country,
            phone: this.state.phone,
            profilepic: this.state.profilepic
        })

        fetch("http://35.204.178.205:4123/users/update", options)
            .then(res => res.json())
            .then(() => {
                let options2 = {}

                options2.method = 'GET'

                options2.headers = {
                    'x-access-token': this.state.account.token
                }

                fetch("http://35.204.178.205:4123/users/detail/" + this.state.account.id, options2).then((res) => res.json()).then((res) => {
                    if (res != null) {
                        if (res.hits.hits.length != 0) {
                            var obj = res.hits.hits;
                            var account = obj[0]._source;
                            account.id = obj[0]._id;
                            account.token = this.state.account.token;
                            localStorage.setItem('account', JSON.stringify(account));
                            localStorage.setItem('nameuser', account.name);
                            console.log("LocalStorage in update -------")
                            console.log(JSON.parse(localStorage.getItem('account')))

                            this.setState({refresh: true});
                        }
                    }
                }).catch(err => console.error(err))

            })
            .catch(err => console.error(err))
            
            this
            .props
            .history
            .push(sessionStorage.getItem("locale") == "ar"
                ? '/profile?locale=ar'
                : '/profile');
    }

    render() {
        return (
            <div
                className="container"
                style={{
                direction: sessionStorage.getItem("locale") === "ar"
                    ? "rtl"
                    : "ltr"
            }}>
                <div
                    className="row"
                    style={{
                    marginBottom: 90,
                    marginTop: 60
                }}>
                    <div className="col-md-12 col-12">
                        <h1
                            style={{
                            fontFamily: "JF Flat Jozoor",
                            textAlign: sessionStorage.getItem("locale") === "ar"
                                ? ('right')
                                : ('left')
                        }}><FormattedMessage id='Editprofile'/></h1>
                    </div>
                </div>

                <div
                    className="row"
                    style={{
                    marginBottom: 10,
                    marginTop: 10
                }}>
                    <div className="col-md-12 col-12">
                        <h1
                            style={{
                            fontFamily: "JF Flat Jozoor",
                            textAlign: sessionStorage.getItem("locale") === "ar"
                                ? ('right')
                                : ('left')
                        }}><FormattedMessage id='Userinformation'/></h1>
                    </div>
                </div>

                <hr/>

                <form action="">
                    <div
                        className="row"
                        style={{
                        fontFamily: "JF Flat Jozoor",
                        marginBottom: 90,
                        marginTop: 60,
                        direction: sessionStorage.getItem("locale") === "ar"
                            ? "rtl"
                            : "ltr"
                    }}>
                        <div className="col-md-4 col-12 d-flex justify-content-center">
                            <Dropzone
                                onDrop={this
                                .onDropProfilePic
                                .bind(this)}
                                className="d-flex align-self-center justify-content-center align-item-center"
                                style={{
                                borderRadius: "50%",
                                width: 160,
                                height: 160
                            }}>

                                <Loader loaded={this.state.loadedProfilePic}>
                                    {this.state.profilepic == ""
                                        ? (<img
                                            src='../../images/defaultAvatar.png'
                                            className="img-responsive rounded-circle align-self-center"
                                            style={{
                                            width: 160,
                                            height: 160,
                                            marginRight: 10,
                                            marginTop: 20,
                                            marginBottom: 20
                                        }}/>)
                                        : (<img
                                            src={this.state.profilepic}
                                            className="img-responsive rounded-circle align-self-center"
                                            style={{
                                            width: 160,
                                            height: 160,
                                            marginRight: 10,
                                            marginTop: 20,
                                            marginBottom: 20
                                        }}/>)
}
                                </Loader>

                            </Dropzone>
                        </div>

                        <div
                            className="col-md-8 col-12 d-flex justify-content-center"
                            style={{
                            paddingRight: 80
                        }}>
                            <div className="align-self-center col-md-12 col-12">

                                <div className="form-group">
                                    <input
                                        className="profileInputTxt"
                                        type="text"
                                        placeholder="Full name"
                                        style={{
                                        fontFamily: "JF Flat Jozoor",
                                        textAlign: sessionStorage.getItem("locale") === "ar"
                                            ? ('right')
                                            : ('left')
                                    }}
                                        value={this.state.name}
                                        onChange={this.handleChange('name')}/>
                                </div>
                                <div className="form-group">
                                    <input
                                        className="profileInputTxt"
                                        type="email"
                                        style={{
                                        fontFamily: "JF Flat Jozoor",
                                        textAlign: sessionStorage.getItem("locale") === "ar"
                                            ? ('right')
                                            : ('left')
                                    }}
                                        placeholder="Email"
                                        value={this.state.email}
                                        onChange={this.handleChange('email')}/>
                                </div>
                                <div className="form-group">
                                    <input
                                        className="profileInputTxt"
                                        type="text"
                                        style={{
                                        fontFamily: "JF Flat Jozoor",
                                        textAlign: sessionStorage.getItem("locale") === "ar"
                                            ? ('right')
                                            : ('left')
                                    }}
                                        placeholder="Gender"
                                        value={this.state.gender}
                                        onChange={this.handleChange('gender')}/>
                                </div>
                                <div className="form-group">
                                    <input
                                        className="profileInputTxt"
                                        type="text"
                                        placeholder="Country"
                                        style={{
                                        fontFamily: "JF Flat Jozoor",
                                        textAlign: sessionStorage.getItem("locale") === "ar"
                                            ? ('right')
                                            : ('left')
                                    }}
                                        value={this.state.country}
                                        onChange={this.handleChange('country')}/>
                                </div>
                                <div className="form-group">
                                    <input
                                        className="profileInputTxt"
                                        type="text"
                                        placeholder="Phone number"
                                        style={{
                                        fontFamily: "JF Flat Jozoor",
                                        textAlign: sessionStorage.getItem("locale") === "ar"
                                            ? ('right')
                                            : ('left')
                                    }}
                                        value={this.state.phone}
                                        onChange={this.handleChange('phone')}/>
                                </div>

                            </div>
                        </div>
                    </div>

                    <hr/>

                    <div
                        className="row"
                        style={{
                        marginTop: 20,
                        marginBottom: 50,
                        paddingLeft: 70,
                        paddingRight: 70
                    }}>
                        <div
                            className="col-md-6 col-12 d-flex align-items-center"
                            style={{
                            marginTop: 10,
                            marginBottom: 10
                        }}>
                            <h3
                                style={{
                                fontFamily: "JF Flat Jozoor",
                                textAlign: sessionStorage.getItem("locale") === "ar"
                                    ? ('right')
                                    : ('left')
                            }}><FormattedMessage id='savechanges'/></h3>
                        </div>
                        <div className="col-md-6 col-12">
                            <div className="float-right">
                                <button
                                    className="btnWhite"
                                    style={{
                                    fontFamily: "JF Flat Jozoor",
                                    marginRight: 30,
                                    marginTop: 10,
                                    marginBottom: 10,
                                    marginRight: sessionStorage.getItem("locale") === "ar"
                                        ? 250
                                        : null
                                }}
                                    onClick={this.cancelupdate}><FormattedMessage id='cancel'/></button>
                                <button
                                    className="btnGreen"
                                    style={{
                                    fontFamily: "JF Flat Jozoor",
                                    marginTop: 10,
                                    marginBottom: 10,
                                    marginRight: sessionStorage.getItem("locale") === "ar"
                                        ? 10
                                        : null,
                                    marginLeft: sessionStorage.getItem("locale") === "ar"
                                        ? null
                                        : 10
                                }}
                                    onClick={this.updateProfile}>
                                    <FormattedMessage id='save'/>
                                </button>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        )
    }
}

export default withRouter(EditProfilePage)
