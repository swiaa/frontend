/**
 * Author : Aymen FEZAI
 */
import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Route, Link, withRouter} from "react-router-dom";

import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import '../../sass/layout/_profilecss.scss'
import '../../images/defaultAvatar.png'

import Switch from "react-switch";
import Dropzone from 'react-dropzone';
import FontAwesome from 'react-fontawesome';
import Loader from 'react-loader';

//For popup messages
import toastr from 'toastr'
import 'toastr/build/toastr.min.css'
import {FormattedMessage} from 'react-intl';

toastr.options = {
    positionClass: 'toast-top-right',
    "closeButton": true,
    hideDuration: 300,
    timeOut: 3000
}
//End of toastr

class EditNotification extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            switch1: true,
            switch2: false
        };
        this.handleChangeswitch1 = this
            .handleChangeswitch1
            .bind(this);
        this.handleChangeswitch2 = this
            .handleChangeswitch2
            .bind(this);
        this._cancel = this
            ._cancel
            .bind(this)

    }
    _cancel(event) {
        event.preventDefault();
        this
            .props
            .history
            .push('/');
    }

    componentWillMount() {
        window.scroll({top: 0, left: 0, behavior: 'smooth'});
    }

    handleChangeswitch1(switch1) {
        this.setState({switch1});
    }

    handleChangeswitch2(switch2) {
        this.setState({switch2});
    }

    handleChange = name => event => {
        this.setState({[name]: event.target.value});
    }

    render() {
        return (
            <div
                className="container"
                style={{
                direction: sessionStorage.getItem("locale") === "ar"
                    ? "rtl"
                    : "ltr"
            }}>
                <div
                    className="row"
                    style={{
                    marginBottom: 10,
                    marginTop: 80
                }}>
                    <div
                        className="col-md-12 col-12"
                        style={{
                        alignItems: sessionStorage.getItem("locale") === "ar"
                            ? "right"
                            : "left"
                    }}>
                        <h1
                            className="mySubTitle"
                            style={{
                            textAlign: sessionStorage.getItem("locale") === "ar"
                                ? ('right')
                                : ('left')
                        }}><FormattedMessage id='notification'/></h1>
                    </div>
                </div>

                <hr/>

                <div
                    className="row"
                    style={{
                    marginBottom: 130,
                    marginTop: 66,
                    paddingLeft: 70,
                    paddingRight: 70
                }}>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-8 col-12">
                                <h3
                                    className="mySmallTitle"
                                    style={{
                                    textAlign: sessionStorage.getItem("locale") === "ar"
                                        ? ('right')
                                        : ('left')
                                }}><FormattedMessage id='dailyreminder'/></h3>
                                <p className="myText"><FormattedMessage id='recievenotifications'/></p>
                            </div>
                            <div className="col-md-4 col-12 float-right">
                                <div className="float-right">
                                    <Switch
                                        onChange={this.handleChangeswitch1}
                                        checked={this.state.switch1}
                                        id="switch1"
                                        uncheckedIcon={<div style = {{ fontFamily: "mbregular", display: "flex", justifyContent: "center", alignItems: "center", height: "100%", fontSize: 14, color: "black", paddingRight: 2 }} > <FormattedMessage id='off'/> </div>}
                                        checkedIcon={<div style = {{ fontFamily: "mbregular", display: "flex", justifyContent: "center", alignItems: "center", height: "100%", fontSize: 14, color: "black", paddingRight: 2 }} > <FormattedMessage id='on'/> </div>}
                                        offColor="#eeeeee"
                                        onColor="#85b898"
                                        offHandleColor="#fff"
                                        onHandleColor="#fff"
                                        height={50}
                                        width={100}/>
                                </div>
                            </div>
                        </div>

                        <div
                            className="row"
                            style={{
                            marginTop: 60
                        }}>
                            <div className="col-md-8 col-12">
                                <h3
                                    className="mySmallTitle"
                                    style={{
                                    textAlign: sessionStorage.getItem("locale") === "ar"
                                        ? ('right')
                                        : ('left')
                                }}><FormattedMessage id='articlereviewresults'/></h3>
                                <p className="myText"><FormattedMessage id='recievenotificationswhenexperts'/></p>
                            </div>
                            <div className="col-md-4 col-12 float-right">
                                <div className="float-right">
                                    <Switch
                                        onChange={this.handleChangeswitch2}
                                        checked={this.state.switch2}
                                        id="switch2"
                                        uncheckedIcon={<div style = {{ fontFamily: "mbregular", display: "flex", justifyContent: "center", alignItems: "center", height: "100%", fontSize: 14, color: "black", paddingRight: 2 }} > <FormattedMessage id='off'/> </div>}
                                        checkedIcon={<div style = {{ fontFamily: "mbregular", display: "flex", justifyContent: "center", alignItems: "center", height: "100%", fontSize: 14, color: "black", paddingRight: 2 }} > <FormattedMessage id='on'/> </div>}
                                        offColor="#eeeeee"
                                        onColor="#85b898"
                                        offHandleColor="#fff"
                                        onHandleColor="#fff"
                                        height={50}
                                        width={100}/>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <hr/>

                <div
                    className="row"
                    style={{
                    marginTop: 20,
                    marginBottom: 50,
                    paddingLeft: 70,
                    paddingRight: 70
                }}>
                    <div
                        className="col-md-6 col-12 d-flex align-items-center"
                        style={{
                        marginTop: 10,
                        marginBottom: 10
                    }}>
                        <h3
                            className="mySmallTitle"
                            style={{
                            textAlign: sessionStorage.getItem("locale") === "ar"
                                ? ('right')
                                : ('left')
                        }}>
                            <FormattedMessage id='savechanges'/></h3>
                    </div>
                    <div className="col-md-6 col-12">
                        <div className="float-right">
                            <button
                                className="btnWhite"
                                style={{
                                fontFamily: "JF Flat Jozoor",
                                marginRight: 30,
                                marginTop: 10,
                                marginBottom: 10,
                                marginRight: sessionStorage.getItem("locale") === "ar"
                                    ? 250
                                    : null
                            }}
                                onClick={this._cancel}>
                                <FormattedMessage id='cancel'/></button>
                            <button
                                className="btnGreen"
                                style={{
                                fontFamily: "JF Flat Jozoor",
                                marginTop: 10,
                                marginBottom: 10,
                                marginRight: sessionStorage.getItem("locale") === "ar"
                                    ? 10
                                    : null,
                                marginLeft: sessionStorage.getItem("locale") === "ar"
                                    ? null
                                    : 10
                            }}>
                                <FormattedMessage id='save'/>
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

export default withRouter(EditNotification)