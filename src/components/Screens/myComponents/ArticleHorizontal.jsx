/**
 * Author : Aymen FEZAI
 */

import React, { Component } from 'react';
import FontAwesome from 'react-fontawesome'
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../../sass/layout/_profilecss.scss';


class ArticleHorizontal extends Component {
    render() {
        return (
            <div className="row articleHorizontal withShadow d-flex" style={{paddingLeft:0, paddingRight:0, marginTop:30}}>
                <div className="col-md-4 col-12 withBGCover imgArticle" style={{backgroundImage: 'url(' + '../images/CategoryCardFashion.png' + ')'}}></div>
                
                <div className="col-md-8 col-12 align-self-center" style={{paddingLeft:20, paddingRight:20, paddingTop:10, paddingBottom:10}}>
                    <div className="row d-flex align-items-center">
                        <div className="col-6 d-flex justify-content-start">
                            <h3 className="mb18Regular" style={{fontWeight:"bold", color:"#3c3d41"}}>Main title</h3>
                        </div>
                        <div className="col-6 d-flex justify-content-end">
                            <button className="btnSmallerBlackish">FASHION</button>
                        </div>
                    </div>

                    <div className="row" style={{marginTop:10, marginBottom:20, paddingLeft:20, paddingRight:20}}>
                        <p className="mb10Regular">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum urna odio, mollis non turpis in, ultrices eleifend ex. </p>
                    </div>
                    <hr />
                    <div className="row d-flex align-items-center" style={{marginTop:10}}>
                        <div className="col-6 d-flex justify-content-start">
                            <h3 className="mb18Regular" style={{fontSize:11, color:"#838d8f"}}>By Author name</h3>
                        </div>
                        <div className="col-6 d-flex justify-content-end">
                            <FontAwesome
                                className='super-crazy-colors'
                                name='bookmark'
                                size='1x'
                                style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)', color:'#838d8f'}}
                            />  
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ArticleHorizontal;