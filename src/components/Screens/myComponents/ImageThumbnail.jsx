/**
 * Author : Aymen FEZAI
 */

import React, { Component } from 'react';
import FontAwesome from 'react-fontawesome'
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../../sass/layout/_profilecss.scss';

 
class ImageThumbnail extends Component {
    render() {
        return (
            <div className="row imgThumbnail withShadow d-flex" style={{paddingLeft:0, paddingRight:0, marginTop:30}}>
                
                <div className="col-md-12 col-12 withBGCover" style={{backgroundImage: 'url(' + '../images/CategoryCardFashion.png' + ')', height:180}}></div>

                <div className="col-md-12 col-12 align-self-center" style={{paddingLeft:20, paddingRight:20, paddingTop:10, paddingBottom:20}}>
                    <p className="mb10Regular">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum urna odio, mollis non turpis in, ultrices eleifend ex. </p>
                </div>
                
            </div>
        );
    }
}

export default ImageThumbnail;