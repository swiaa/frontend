import React, {Component} from 'react';
import axios from 'axios';
import {BrowserRouter as Router, Route, Link, withRouter} from "react-router-dom";

class ActivationPage extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: props.match.params.id
        };

        this._validate = this
            ._validate
            .bind(this);
    }

    componentWillMount() {
        window.scroll({top: 0, left: 0, behavior: 'smooth'});
    }

    _validate(event) {
        event.preventDefault();

        let body = {};

        body = {
            id: this.state.id,
            mailvalidation: true
        }

        axios
            .post("http://127.0.0.1:3000/users/validateEmail", body)
            .then(res => {
                if (res.status === 200) {
                    this
                        .props
                        .history
                        .push('/')
                }
            })
            .catch(err => console.error(err))
    }

    render() {
        return (
            <div>
                <div
                    className="container"
                    style={{
                    marginTop: 80
                }}>
                    <div className="row">
                        <div className="col-md-6 col-6 offset-3 d-flex justify-content-center">
                            <h1 className="myTitle">Congrats, Your account has been activated</h1>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6 col-6 offset-3 d-flex justify-content-center">
                            <h1 className="mySmallTitle">Please click the button bellow to proceed</h1>
                        </div>
                    </div>
                    <div
                        className="row"
                        style={{
                        marginTop: 30
                    }}>
                        <div className="col-md-6 col-6 offset-3 d-flex justify-content-center">
                            <button className="btnGreen" onClick={this._validate}>Proceed</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(ActivationPage);