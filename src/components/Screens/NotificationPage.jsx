import React, {Component} from 'react';
import Test from '../Parts/Footer.jsx'

import {Pagination, PaginationItem, PaginationLink} from 'reactstrap';
import {FormattedMessage} from 'react-intl';

class NotificationPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            userName: "",
            userPic: "",

            notifications: [],

            tabIndex: 0,
            currentPageNotif: 1,
            pagesNotif: 1
        }

        this._getNotifications = this
            ._getNotifications
            .bind(this);
        this._readNotification = this
            ._readNotification
            .bind(this);
    }

    componentDidMount() {
        window.scroll({top: 0, left: 0, behavior: 'smooth'});
    }

    _getNotifications(pageIndex) {
        
    }

    _readNotification(notification_id) {
        
    }

    handlePageClickNotif(pageIndex) {
        window.scroll({top: 0, left: 0, behavior: 'smooth'});
        this.setState({currentPageDocument: pageIndex, loaded: false});
        this._getNotifications(pageIndex);
    }

    render() {
        let pagination1 = [];
        for (let pageIndex = 1; pageIndex <= this.state.pagesNotif; pageIndex++) {
            pagination1.push(
                <PaginationItem
                    key={pageIndex}
                    active={this.state.currentPageNotif == pageIndex
                    ? true
                    : false}>
                    <PaginationLink onClick={() => this.handlePageClickNotif(pageIndex)}>
                        {pageIndex}
                    </PaginationLink>
                </PaginationItem>
            );
        }

        let notificationsItems = [];

            for (let i = 0; i < 5; i++) {
                let divClass = "";
                let cardClass = "";

                if (i%2 == 0) {
                        divClass = "card mySuccessCard border-success mb-3"
                    cardClass = "card-body text-success"
                } else {
                        divClass = "card myDangerCard border-danger mb-3"
                    cardClass = "card-body text-danger"
                }

                notificationsItems.push(
                    <div
                        key={i}
                        className="row"
                        style={{
                        marginBottom: 20,
                        marginTop: 20,
                        cursor: "pointer"
                    }}
                        onClick={() => this._readNotification(this.state.notifications[i]._id)}>
                        <div
                            className={divClass}
                            style={{
                            width: "100%",
                            float: "right"
                        }}>
                            <div className={cardClass}>
                                <div
                                    style={{
                                    float: "left"
                                }}>
                                <img
                                            src='../../images/defaultAvatar.png'
                                            className="img-responsive rounded-circle align-self-center"
                                            style={{
                                            width: 50,
                                            height: 50,
                                            marginRight: 20
                                        }}/>


                                </div>
                                <div>
                                    <p
                                        className="card-text"
                                        style={{
                                        fontWeight: "bold",
                                        marginBottom: "10px"
                                    }}>Author : Foulen
                                    </p>
                                    <p
                                        className="card-text"
                                        style={{
                                        marginBottom: "10px"
                                    }}>submited "this job"</p>
                                </div>
                            </div>
                        </div>
                    </div>
                )
        }

        return (
            <div>

                <div className="container">
                    <div
                        className="row"
                        style={{
                        marginBottom: 30,
                        marginTop: 80
                    }}>
                        <div className="col-md-12 col-12">
                            <h1
                                className="mySubTitle"
                                style={{
                                textAlign: sessionStorage.getItem("locale") == "ar"
                                    ? "right"
                                    : "left"
                            }}><FormattedMessage id='Notifications'/></h1>
                        </div>
                    </div>
                    <hr
                        style={{
                        marginBottom: 20,
                        marginTop: 20
                    }}/>

                    <div className="container">

                        {notificationsItems}

                    </div>

                    <div
                        className="row d-flex justify-content-center"
                        style={{
                        paddingBottom: 50
                    }}>
                        <Pagination>
                            <PaginationItem
                                disabled={this.state.currentPageNotif == 1
                                ? true
                                : false}>
                                <PaginationLink
                                    previous
                                    onClick={() => this.handlePageClickNotif(this.state.currentPageNotif - 1)}/>
                            </PaginationItem>
                            {pagination1}
                            <PaginationItem
                                disabled={this.state.currentPageNotif == this.state.pagesNotif
                                ? true
                                : false}>
                                <PaginationLink
                                    onClick={() => this.handlePageClickNotif(this.state.currentPageNotif + 1)}
                                    next/>
                            </PaginationItem>
                        </Pagination>
                    </div>
                </div>

                <Test/>
            </div>
        );
    }
}

export default NotificationPage;
