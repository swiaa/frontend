import React from 'react'

import '../../sass/layout/_profilecss.scss'
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import '../../images/demenagement.jpg'
import '../../images/user.png'
import Footer from '../Parts/Footer.jsx'

import {Map, InfoWindow, Marker, GoogleApiWrapper, GoogleMap} from 'google-maps-react';
import {relative} from 'path';

class Job extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            btnState: true
        }
        this.goNext = this
            .goNext
            .bind(this)
    }

    componentWillMount() {
        window.scroll({top: 0, left: 0, behavior: 'smooth'});
    }

    goNext() {
        let oldState = this.state.btnState

        this.setState({
            btnState: !oldState
        })
    }

    render() {

        return (
            <div>
                <div
                    className="container"
                    style={{
                    paddingTop: 60
                }}>
                    <div
                        className="row"
                        style={{
                        marginBottom: 10
                    }}>
                        <div className="col-md-6 col-12">
                            <div
                                className="row"
                                style={{
                                marginBottom: 10
                            }}>
                                <div className="col-6 d-flex justify-content-start">
                                    <span
                                        className="myTitle"
                                        style={{
                                        color: "black"
                                    }}>Title</span>
                                </div>
                                <div className="col-6 d-flex justify-content-end">
                                    <span
                                        className="myTitle"
                                        style={{
                                        color: "black"
                                    }}>10 Dt</span>
                                </div>
                            </div>
                            <div
                                className="row d-flex align-items-center"
                                style={{
                                marginBottom: 10
                            }}>
                                <div className="col-md-2 col-6">
                                    <img
                                        src="../../images/user.png"
                                        className="img-responsive rounded-circle align-self-center"
                                        style={{
                                        width: 60,
                                        height: 60,
                                        marginRight: 10,
                                        marginTop: 20,
                                        marginBottom: 20
                                    }}/>
                                </div>
                                <div className="col-md-10 col-6">
                                    <span className="mb20SemiBold d-flex align-items-center">Foulen</span>
                                </div>
                            </div>
                            <div
                                className="row"
                                style={{
                                marginBottom: 10
                            }}>
                                <div className="col-12">
                                    <p
                                        className="mb18Regular"
                                        style={{
                                        color: "black"
                                    }}>
                                        Eu anim anim quis qui ut Lorem tempor consequat dolore. Minim magna deserunt
                                        ipsum exercitation. Ad duis labore nostrud et et non eu duis. Veniam duis
                                        commodo qui labore cupidatat enim veniam dolor ipsum ipsum sint anim. Lorem
                                        laboris sunt velit id do laboris exercitation fugiat proident tempor enim
                                        cupidatat.
                                    </p>
                                </div>
                            </div>
                            <div
                                className="row d-flex justify-content-center"
                                style={{
                                marginTop: 40,
                                marginBottom: 20
                            }}>

                                {(this.state.btnState)
                                    ? (
                                        <button
                                            type="submit"
                                            className="btnGreen"
                                            onClick={this.goNext}
                                            style={{
                                            height: 50,
                                            width: 200
                                        }}>Submit</button>
                                    )
                                    : (
                                        <button
                                            type="submit"
                                            className="btnGreen"
                                            onClick={this.goNext}
                                            style={{
                                            height: 50,
                                            width: 200,
                                            backgroundColor: "green",
                                            color: "white"
                                        }}>Pending...</button>
                                    )
}

                            </div>

                        </div>
                        <div className="col-md-6 col-12">
                            <img
                                src='../../images/demenagement.jpg'
                                style={{
                                objectFit: "contain",
                                height: 400
                            }}/>
                        </div>
                    </div>

                </div>

                <div
                    className="container-fluid"
                    style={{
                    position: relative,
                    marginTop: 30,
                    bottom: 0
                }}>
                    <div className="row">
                        <Map
                            google={this.props.google}
                            style={{
                            width: '100%',
                            height: '400px',
                            position: 'relative'
                        }}
                            className={'map'}
                            zoom={14}>
                            <Marker
                                title={'The marker`s title will appear as a tooltip.'}
                                name={'SOMA'}
                                position={{
                                lat: 37.778519,
                                lng: -122.405640
                            }}/>
                        </Map>
                    </div>
                </div>

            </div>
        )

    }

}

export default GoogleApiWrapper({apiKey: ('AIzaSyCdHlVlOBMyR9AD2BHbezfCpgT4B9BYrVU')})(Job)
