/**
 * Author : Aymen FEZAI
 */
import React from 'react'
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import '../../sass/layout/_profilecss.scss'
import Dropzone from 'react-dropzone'
import FontAwesome from 'react-fontawesome'
import '../../sass/layout/react-tagsinput.css'
import '../../images/upload.png';
import '../../images/Mediaupload.png';
import {BrowserRouter as Router, Route, Link, withRouter} from "react-router-dom";
import {Parallax, Background} from 'react-parallax';
import PlacesAutocomplete, {geocodeByAddress, getLatLng} from 'react-places-autocomplete';
import Switch from "react-switch";
import {FormattedMessage} from 'react-intl';

//For popup messages
import toastr from 'toastr'
import 'toastr/build/toastr.min.css'

toastr.options = {
    positionClass: 'toast-top-right',
    "closeButton": true,
    hideDuration: 300,
    timeOut: 3000
}
//End of toastr

import {CardBody, Col, Form, FormGroup, Row} from 'reactstrap';

class NewJobPage extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            category: "",
            title: "",
            description: "",
            thumbnail: "",
            address: '',
            switch1: true,
            coast: ''
        };

        this.handleChangeswitch1 = this
            .handleChangeswitch1
            .bind(this);

    }

    componentWillMount() {
        window.scroll({top: 0, left: 0, behavior: 'smooth'});
        this.setState({
            account: JSON.parse(localStorage.getItem('account'))
        });
    }

    handleChangeswitch1(switch1) {
        this.setState({switch1});
    }

    handleChange = name => event => {
        this.setState({[name]: event.target.value});
    };

    onDropThumbnail(thumbnail) {

        this.setState({loadedModal: false});

        let options = {};

        var data = new FormData();

        data.append('file', thumbnail[0]);
        options.body = data;

        options.headers = {
            'x-access-token': this.state.account.token
        };

        options.method = 'POST';

        fetch("http://35.204.178.205:4123/flavor/uploadonefile", options).then((res) => res.json()).then((res) => {
            if (res != null) {
                toastr.clear();
                toastr.success("Media has been added!");

                this.setState({thumbnail: res, loadedModal: true});
            }
        }).catch(err => console.error(err))

    }

    handleChangeAddress = address => {
        this.setState({address});
    };

    handleSelect = address => {
        this.setState({address})

        geocodeByAddress(address)
            .then(results => getLatLng(results[0]))
            .then(latLng => {
                //this.setState({location : [ latLng.lat, latLng.lng]});
                this.setState({location: address});
            })
            .catch(error => console.error('Error', error));
    };

    render() {
        return (
            <div
                className="container"
                style={{
                fontFamily: "JF Flat Jozoor",
                direction: sessionStorage.getItem("locale") === "ar"
                    ? "rtl"
                    : "ltr"
            }}>
                <div
                    className="row"
                    style={{
                    marginBottom: 90,
                    marginTop: 60
                }}>
                    <div className="col-md-12 col-12">
                        <h1
                            className="myTitle"
                            style={{
                            textAlign: sessionStorage.getItem("locale") === "ar"
                                ? ('right')
                                : ('left'),
                            textAlign: 'center'
                        }}><FormattedMessage id='newJob'/></h1>
                    </div>
                </div>

                <Row>
                    <Col>
                        <CardBody>
                            <Form
                                style={{
                                marginBottom: 20
                            }}>
                                <FormGroup row>
                                    <Col md="2">
                                        <h1
                                            className="myTitleT"
                                            style={{
                                            textAlign: sessionStorage.getItem("locale") === "ar"
                                                ? ('right')
                                                : ('left'),
                                            marginBottom: 20
                                        }}><FormattedMessage id='choosecategory'/></h1>
                                    </Col>
                                    <Col xs="12" md="10">
                                        <select
                                            name="category"
                                            id="category"
                                            style={{
                                            height: 50,
                                            paddingTop: 15
                                        }}
                                            className="inputSelect myInputText"
                                            value={this.state.gender}
                                            onChange={this.handleChange('category')}
                                            ref="gender">
                                            <option value="bricolage">Bricolage</option>
                                            <option value="demenagement">Déménagement</option>
                                            <option value="education">Education</option>
                                        </select>
                                    </Col>
                                </FormGroup>

                                <FormGroup row>
                                    <Col md="2">
                                        <h1
                                            className="myTitleT"
                                            style={{
                                            textAlign: sessionStorage.getItem("locale") === "ar"
                                                ? ('right')
                                                : ('left'),
                                            marginBottom: 20
                                        }}>Cover</h1>
                                    </Col>
                                    <Col xs="12" md="10">
                                        {(this.state.thumbnail == "")
                                            ? (

                                                <Dropzone
                                                    onDrop={this
                                                    .onDropThumbnail
                                                    .bind(this)}
                                                    className="dropZone d-flex justify-content-center"
                                                    accept="image/jpeg, image/png">
                                                    <div className="align-self-center justify-content-center text-center">

                                                        <FontAwesome
                                                            className='super-crazy-colors'
                                                            name='camera'
                                                            size='3x'
                                                            style={{
                                                            textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)',
                                                            color: '#838d8f',
                                                            marginBottom: 40
                                                        }}/>
                                                        <p className="myInputText text-center">
                                                            <FormattedMessage id='jobThumbnail'/>
                                                        </p>
                                                        <p className="mb10Regular text-center">
                                                            <FormattedMessage id='pngjpeg'/>
                                                        </p>
                                                    </div>
                                                </Dropzone>
                                            )
                                            : (
                                                <div>
                                                    <Dropzone
                                                        onDrop={this
                                                        .onDropThumbnail
                                                        .bind(this)}
                                                        className="dropZone1 d-flex justify-content-center"
                                                        accept="image/jpeg, image/png">
                                                        <FontAwesome
                                                            className='super-crazy-colors'
                                                            name='camera'
                                                            size='3x'
                                                            style={{
                                                            textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)',
                                                            color: '#838d8f',
                                                            marginBottom: 10
                                                        }}/>
                                                    </Dropzone>
                                                    <Parallax
                                                        bgImage={this.state.thumbnail}
                                                        blur={2}
                                                        style={{
                                                        marginRight: 90,
                                                        width: '100%'
                                                    }}>
                                                        <div className="d-flex justify-content-center align-items-center imgstyle">
                                                            <img
                                                                src={this.state.thumbnail}
                                                                alt=""
                                                                style={{
                                                                objectFit: "cover",
                                                                height: 200,
                                                                width: '100%'
                                                            }}/>
                                                        </div>
                                                    </Parallax>
                                                </div>
                                            )
}

                                    </Col>
                                </FormGroup>

                                <FormGroup row>
                                    <Col md="2">
                                        <h1
                                            className="myTitleT"
                                            style={{
                                            textAlign: sessionStorage.getItem("locale") === "ar"
                                                ? ('right')
                                                : ('left')
                                        }}><FormattedMessage id='jobTitle'/></h1>
                                    </Col>
                                    <Col xs="12" md="10">
                                        <FormattedMessage id='jobTitle'>
                                            {placeholder => (<input
                                                type="text"
                                                className="inputSelectAdd myInputText"
                                                style={{
                                                color: 'black',
                                                width: '100%',
                                                fontFamily: "JF Flat Jozoor",
                                                height: 40,
                                                textAlign: sessionStorage.getItem("locale") === "ar"
                                                    ? ('right')
                                                    : ('left')
                                            }}
                                                placeholder={placeholder}
                                                value={this.state.title}
                                                onChange={this.handleChange('title')}
                                                ref="title"/>)}
                                        </FormattedMessage>
                                    </Col>
                                </FormGroup>

                                <FormGroup row>
                                    <Col md="2">
                                        <h1
                                            className="myTitleT"
                                            style={{
                                            textAlign: sessionStorage.getItem("locale") === "ar"
                                                ? ('right')
                                                : ('left')
                                        }}><FormattedMessage id='jobhere'/></h1>
                                    </Col>
                                    <Col xs="12" md="10">
                                        <FormattedMessage id='jobherecontentHint'>
                                            {placeholder => (<textarea
                                                className="inputSelectAdd myInputText"
                                                style={{
                                                color: 'black',
                                                width: '100%',
                                                padding: 15,
                                                height: 150,
                                                textAlign: sessionStorage.getItem("locale") === "ar"
                                                    ? ('right')
                                                    : ('left')
                                            }}
                                                placeholder={placeholder}
                                                value={this.state.description}
                                                onChange={this.handleChange('description')}
                                                ref="description"/>)}

                                        </FormattedMessage>
                                    </Col>
                                </FormGroup>
                            </Form>

                            <FormGroup row>
                                <Col md="2">
                                    <h1
                                        className="myTitleT"
                                        style={{
                                        textAlign: sessionStorage.getItem("locale") === "ar"
                                            ? ('right')
                                            : ('left')
                                    }}>Place</h1>
                                </Col>
                                <Col xs="12" md="10">
                                    <PlacesAutocomplete
                                        value={this.state.address}
                                        onChange={this.handleChangeAddress}
                                        onSelect={this.handleSelect}>
                                        {({getInputProps, suggestions, getSuggestionItemProps, loading}) => (
                                            <div>
                                                <input
                                                    {...getInputProps({ style:{color:'black',height:40, marginLeft:5, width:"100%",textAlign:sessionStorage.getItem("locale")==="ar"? ('right'):('left')}, className: 'location-search-input inputSelectAdd myInputTextListItem', })}/>
                                                <div className="autocomplete-dropdown-container myInputText">
                                                    {loading && <div>Loading...</div>}
                                                    {suggestions.map(suggestion => {
                                                        const className = suggestion.active
                                                            ? 'suggestion-item--active'
                                                            : 'suggestion-item';
                                                        const style = suggestion.active
                                                            ? {
                                                                backgroundColor: '#fafafa',
                                                                cursor: 'pointer'
                                                            }
                                                            : {
                                                                backgroundColor: '#ffffff',
                                                                cursor: 'pointer'
                                                            };
                                                        return (
                                                            <div {...getSuggestionItemProps(suggestion, { className, style, })}>
                                                                <span>{suggestion.description}</span>
                                                            </div>
                                                        );
                                                    })}
                                                </div>
                                            </div>
                                        )}
                                    </PlacesAutocomplete>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Col md="2">
                                    <h1
                                        className="myTitleT"
                                        style={{
                                        textAlign: sessionStorage.getItem("locale") === "ar"
                                            ? ('right')
                                            : ('left')
                                    }}>Coast</h1>
                                </Col>
                                <Col xs="8" md="8">
                                    <FormattedMessage id='coast'>
                                        {placeholder => (<input
                                            type="text"
                                            className="inputSelectAdd myInputText"
                                            style={{
                                            color: 'black',
                                            width: '100%',
                                            fontFamily: "JF Flat Jozoor",
                                            height: 40,
                                            textAlign: sessionStorage.getItem("locale") === "ar"
                                                ? ('right')
                                                : ('left')
                                        }}
                                            placeholder={placeholder}
                                            value={this.state.coast}
                                            onChange={this.handleChange('coast')}
                                            ref="coast"
                                            disabled = {
                                               (!this.state.switch1)?(true):(false)
                                            }/>
                                            )}
                                    </FormattedMessage>
                                </Col>
                                <div className="col-md-2 d-flex justify-content-end">
                                    <Switch
                                        onChange={this.handleChangeswitch1}
                                        checked={this.state.switch1}
                                        id="switch1"
                                        uncheckedIcon={
                                            <div style = {{ fontFamily: "mbregular", display: "flex", justifyContent: "center", alignItems: "center", height: "100%", fontSize: 14, color: "black", paddingRight: 2 }} > 
                                                Free 
                                            </div>
                                        }
                                        checkedIcon={
                                            <div style = {{ fontFamily: "mbregular", display: "flex", justifyContent: "center", alignItems: "center", height: "100%", fontSize: 14, color: "black", paddingRight: 2 }} > 
                                                Payed
                                            </div>
                                        }
                                        offColor="#eeeeee"
                                        onColor="#F0E40F"
                                        offHandleColor="#fff"
                                        onHandleColor="#fff"
                                        height={40}
                                        width={100}/>
                                </div>
                            </FormGroup>

                            <FormGroup row>
                                <Col md="1"></Col>
                                <Col>
                                    <div className="row align-self-center justify-content-center text-center">
                                        <button
                                            type="submit"
                                            className="btnGreen"
                                            onClick={this.goNext}
                                            style={{
                                            height: 50,
                                            width: 150
                                        }}>Submit</button>
                                    </div>
                                </Col>
                            </FormGroup>
                        </CardBody>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default withRouter(NewJobPage)
