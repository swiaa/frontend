import React from 'react'

import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import "../../images/headerImage.jpg";
import Footer from './../Parts/Footer.jsx'
import Header from './../Parts/header.jsx'
import Explorecategory from './../Parts/Explorecategory.jsx'
import TodayCollection from './../Parts/TodayCollection.jsx'
import {Parallax, Background} from 'react-parallax';
import {FormattedMessage, injectIntl} from 'react-intl';

var Loader = require('react-loader');

export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            keyword: ""
        }
    }

    componentWillMount() {
        window.scroll({top: 0, left: 0, behavior: 'smooth'});
    }

    handleChange = name => event => {
        this.setState({[name]: event.target.value});
    };

    render() {
        return (
            <div>
                {(JSON.parse(localStorage.getItem('account')))
                    ? (
                        <Parallax bgImage="../../images/headerImage.jpg" bgStyle={{backgroundPosition: "center"}} strength={200}>
                            <div
                                style={{
                                paddingTop: 60,
                                paddingBottom: 60
                            }}>
                                <form action="">
                                    <div
                                        className="row col-md-12 align-items-center"
                                        style={{
                                        height: 180
                                    }}>

                                        <div className="col-md-3"></div>
                                        <div
                                            className="row col-md-6"
                                            style=
                                            {{ height: 62, borderRadius: 6, backgroundColor: "rgba(255, 255, 255, 0.6)", paddingTop:11, direction: sessionStorage.getItem("locale")==="ar"? "rtl":"ltr" }}>
                                            <div className="col">
                                                <input
                                                    type="text"
                                                    className="inputSelectAdd myInputText"
                                                    style={{
                                                    color: 'black',
                                                    width: '100%',
                                                    fontFamily: "JF Flat Jozoor",
                                                    height: 40,
                                                    textAlign: sessionStorage.getItem("locale") === "ar"
                                                        ? ('right')
                                                        : ('left')
                                                }}
                                                    placeholder="Search for a job..."
                                                    value={this.state.keyword}
                                                    onChange={this.handleChange('keyword')}
                                                    ref="keyword"/>
                                            </div>
                                            <div
                                                className="col-2"
                                                align="right"
                                                style={{
                                                padding: 0
                                            }}>
                                                <button
                                                    type="submit"
                                                    className="searchbtn"
                                                    style={{
                                                    marginTop: 0,
                                                    width: "100%",
                                                    height: 40
                                                }}><FormattedMessage id='search'/></button>
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </Parallax>
                    )
                    : (
                        <div>
                            <Header
                                dimension={{
                                d1: 550,
                                d2: 263
                            }}/>
                            <Explorecategory/>
                        </div>
                    )
}

                <TodayCollection/>
                <Footer/>

            </div>
        )
    }
}
