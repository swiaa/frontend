/**
 * Author : Aymen FEZAI
 */

import React from 'react'
import {BrowserRouter as Router, Route, Link, withRouter} from "react-router-dom";
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import '../../sass/layout/_profilecss.scss'
import '../../images/defaultAvatar.png'
import Footer from '../Parts/Footer.jsx'

//For popup messages
import toastr from 'toastr'
import 'toastr/build/toastr.min.css'
import {FormattedMessage} from 'react-intl';
toastr.options = {
    positionClass: 'toast-top-right',
    "closeButton": true,
    hideDuration: 300,
    timeOut: 3000
}
//End of toastr

const css_name = {
    fontFamily: "JF Flat Jozoor",
    fontSize: 28,
    fontWeight: "bold",
    fontStyle: "normal",
    textAlign: "left",
    color: "#3c3d41",
    marginRight: 10,
    marginTop: 20,
    marginBottom: 20
};

class ProfilePage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            account: {}
        }

        this.goToEdit = this
            .goToEdit
            .bind(this);
    }

    componentDidMount() {
        console.log(this.state.account)
    }

    goToEdit(event) {
        event.preventDefault();

        this
            .props
            .history
            .push({
                pathname: sessionStorage.getItem('locale') === 'ar'
                    ? '/editProfile/?locale=ar'
                    : '/editProfile'
            })

    }

    render() {

        return (
            <div>

                <div className="container">

                    <div className="hidden-sm-down">
                        <div
                            className="row"
                            style={{
                            fontFamily: "JF Flat Jozoor",
                            marginBottom: 80,
                            marginTop: 80,
                            direction: sessionStorage.getItem("locale") === "ar"
                                ? "rtl"
                                : "ltr"
                        }}>

                            <div className="col-md-6">
                                <div className="row">
                                    <div className="col-md-6 col-12 d-flex justify-content-center">

                                        {this.state.account.profilepic == ""
                                            ? (<img
                                                src='../../images/defaultAvatar.png'
                                                className="img-responsive rounded-circle align-self-center"
                                                style={{
                                                width: 160,
                                                height: 160,
                                                marginRight: 10,
                                                marginTop: 20,
                                                marginBottom: 20
                                            }}/>)
                                            : (<img
                                                src={this.state.account.profilepic}
                                                className="img-responsive rounded-circle align-self-center"
                                                style={{
                                                width: 160,
                                                height: 160,
                                                marginRight: 10,
                                                marginTop: 20,
                                                marginBottom: 20
                                            }}/>)}

                                    </div>
                                    <div className="col-md-6 col-12 d-flex justify-content-center">
                                        <h1 style={css_name} className="align-self-center">{this.state.account.name}</h1>
                                    </div>
                                </div>
                            </div>

                            <div
                                className="col-md-3 offset-md-3 col-12 float-right d-flex justify-content-center">
                                <button
                                    className="pProfilebtnEditProfile align-self-center"
                                    style={{
                                    fontFamily: "JF Flat Jozoor",
                                    marginTop: 20,
                                    marginBottom: 20
                                }}
                                    onClick={this.goToEdit}>
                                    <FormattedMessage id='Editprofile'/>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <Footer/>
            </div>

        );
    }
}

export default withRouter(ProfilePage)
