import React from 'react'
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import '../../sass/layout/_profilecss.scss'
import {
    Pagination, 
    PaginationItem, 
    PaginationLink
} from 'reactstrap';

import {emptyimagemyarticles} from '../../images/my_articles_loading_state.png'
import Modal from 'react-modal';
import { BrowserRouter as Router, Route, Link ,withRouter} from "react-router-dom";
import {FormattedMessage} from 'react-intl';
//For popup messages
import toastr from 'toastr'
import 'toastr/build/toastr.min.css'

toastr.options = {
    positionClass : 'toast-top-right',
    "closeButton": true,
    hideDuration: 300,
    timeOut: 3000
}
//End of toastr

//Modal Style
const customStyles = {
    content : {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        width: '50%',
        maxHeight: "100vh",
        opacity: 1,
        zIndex: 999
    },
    overlay: {
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        zIndex: 999
    },
};

class MyArticlesPage extends React.Component {

    constructor(props) {
        super(props);
    
        this.state = {
          title : '',
          description : '',
          keywords : '',
          ListTracks : [],
          account : {},
          loaded : true,
          currentPage: 1,
          Pages: null,

          modalIsOpen: false,
            
          itemID : "",
          itemIndex : "",
          
          status : "", //waiting || in draft
        }
        this._action = this._action.bind(this);
        this._allArticles = this._allArticles.bind(this);
        this._allSavedDrafts = this._allSavedDrafts.bind(this);
        this._allArticlesSubmitted = this._allArticlesSubmitted.bind(this);
        this._update = this._update.bind(this);
        this._delete = this._delete.bind(this);

        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.createarticle=this.createarticle.bind(this);
    
      }


    componentWillMount(){
        window.scroll({top: 0, left: 0, behavior: 'smooth'});
        this.setState({
            account: JSON.parse(localStorage.getItem('account')),
            //status : "Waiting ..."
            status:"Waiting for approval"
        })
    }

    componentDidMount(){
        _allArticles(0)
    }

    createarticle(){
        this.props.history.push('/newArticle') ;
        }

    _allArticles(pageIndex)
    {
        this.refs.btnAllArticles.classList.add('active')
        this.refs.btnAllSavedDrafts.classList.remove('active')
        this.refs.btnAllArticlesSubmitted.classList.remove('active')
        this._action("http://35.204.178.205:4123/categorization/getCustomListPerUserCat",false,pageIndex) ;
        this.setState({
            //status : "Waiting ..."
            status:"Waiting for approval"
     
        })
    }

    _allArticlesSubmitted()
    {
        this.refs.btnAllArticles.classList.remove('active')
        this.refs.btnAllSavedDrafts.classList.remove('active')
        this.refs.btnAllArticlesSubmitted.classList.add('active')
        this._action("http://35.204.178.205:4123/categorization/getCustomListPerUserCat",true,this.state.currentPage) ;
        this.setState({
            //status : "Waiting ..."
            status:"Waiting for approval"
     
        })
    }

    _allSavedDrafts()
    {
        this.refs.btnAllArticles.classList.remove('active')
        this.refs.btnAllSavedDrafts.classList.add('active')
        this.refs.btnAllArticlesSubmitted.classList.remove('active')
        this._action("http://35.204.178.205:4123/categorization/getCustomListPerUserCatDraft",false,this.state.currentPage) ;
        this.setState({
            //status : "In draft"
            status: <FormattedMessage id='Indraft'/> 
     
        })
    }

    _update(id, index)
    {
        let draft = false
        if (index == "categorization_draft"){
            draft = true;
        }else{
            draft = false;
        }

        //this.props.history.push('/newArticle',{ actionUpdate : true , idarticleToUpdate : id , draft}) ;

       this.props.history.push({ pathname:sessionStorage.getItem('locale')==='ar'?'/newArticle?locale=ar' :'/newArticle'},{ actionUpdate : true , idarticleToUpdate : id , draft});
       location.reload();
    }

    _action(link,submitted,pageIndex)
    {
        this.setState({loaded:false});
        let options = {};

        options.method = 'POST';

        options.headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'x-access-token' : 'gd654dg@fuyffd5'
        };

        if(submitted == true){
            options.body = JSON.stringify({
                IDUSER :  this.state.account.id,
                paginationStart : (pageIndex-1)*10,
                paginationEnd: 10,
                submitted : true
            });
        }else{
            options.body = JSON.stringify({
                IDUSER :  this.state.account.id,
                paginationStart : (pageIndex-1)*10,
                paginationEnd: 10,
            });
        }
                  
        fetch(link, options)
            .then((response) => response.json())
            .then((response) => {
                
                console.log("MyArticlePage : Response -----------------------")
                console.log(response)

                if(response != 'null'){            
                    var pages = Math.ceil(response.total/10);

                    this.setState({Pages: pages, currentPage: pageIndex});

                    var mainlist = response.hits;
                    var listItems = [] ;
                    var listItems = mainlist.map((item, i) =>
                    
                        <tr key={i}>
                            <td><img src={item._source.thumbnail} className="img-responsive align-self-center" style={{width:120, height:120}}/></td>
                            <td><h3 className="myText">{item._source.title}</h3></td>
                            <td><h6 className="myText">{item._source.typecat}</h6></td>
                            <td> 
                                { 
                                    ((item._source.confirmed == false) || (item._source.confirmed == 'false'))
                                    ?
                                    <div>
                                        {
                                            ((!item._source.rejected) || (item._source.rejected === false) || (item._source.rejected == 'false') )?(
                                                
                                                <div style={{flexDirection:'row',marginTop:10}} >
                                                {
                                                    (this.state.status=="Waiting for approval")?
                                                    ( <div style={{float:'left',marginRight:10,color:'red'}}>
                                                    {this.state.status}
                                                </div>):( 
                                        
                                                <div style={{float:'left',marginRight:10,color:'orange',fontSize:15,fontFamily:'JF Flat Jozoor'}}>
                                                    {this.state.status}
                                                </div>)
                                                }
                                    
                                                </div>
                                            ):(
                                                <div style={{flexDirection:'row',marginTop:10}} >
                                                    <div style={{float:'left',marginRight:10,color:'red'}}>
                                                    <FormattedMessage id='RejectedbyExpert'/>  
                                                    </div> 
                                                </div>
                                            ) 
                                        }
                                    </div>
                                    :
                                    <div style={{color:'green'}}>
                                        <strong><FormattedMessage id='Confirmed'/><span style={{color:'red'} }></span></strong>
                                    </div>
                                }
                            </td>
                            <td >
                            <div style={{width:'100%'}}>
                                <button type="button" className="btn btnGreen" onClick={()=>{this._update(item._id, item._index)}} style={{marginRight:5,marginTop:5}}><FormattedMessage id='Update'/></button>
                                <button type="button" className="btn btnPink" onClick={()=>{this.openModal(item._id, item._index)}}style={{marginRight:5,marginTop:5}}><FormattedMessage id='Delete'/></button>
                                </div>
                            </td>
                        </tr>
                    );
    
                    this.setState({ListTracks : listItems});
                    this.setState({loaded : true});
                }
            })
            .catch((error) => {
                console.error(error);
                // TODO
            });
    }

    _delete(){
        let id = this.state.itemID;
        let index = this.state.itemIndex;

        let options = {};

        options.method = 'DELETE';

        options.headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'x-access-token' : this.state.account.token
        };

        options.body = JSON.stringify({
            index: index,
            type: "all"
        });

        fetch("http://35.204.178.205:4123/categorization/deleteArticle/"+id, options)
            .then((response) => response.json())
            .then((response) => {
                if (response.result){
                    toastr.success("Article has been deleted!");
                }else{
                    toastr.error("Something went wrong");
                }
                location.reload();
            })
            .catch(error => console.error(error));

    }

    handlePageClick(pageIndex){
        // console.log("index: "+ pageIndex);
        this.setState({currentPage: pageIndex, loaded: false});
        this._allArticles(pageIndex);
    }

    componentDidMount()
    {

        this._action("http://35.204.178.205:4123/categorization/getCustomListPerUserCat",false,this.state.currentPage) ;

    }

    openModal(itemID, itemIndex){
        this.setState({
            itemID,
            itemIndex
        })

        this.setState({modalIsOpen: true});
    }

    closeModal() {
        this.setState({modalIsOpen: false});
    }


    closeModal() {
        this.setState({
            itemID : "",
            itemIndex : ""
        })

        this.setState({modalIsOpen: false});
    }

    render() {
        let pagination = [];
        
        for (let pageIndex=1; pageIndex <= this.state.Pages; pageIndex++) { // use let here to avoid closure
        pagination.push(
            <PaginationItem
                key={pageIndex}
                active={this.state.currentPage == pageIndex ? true: false}
            >
                <PaginationLink onClick={()=>this.handlePageClick(pageIndex)}>
                    {pageIndex}
                </PaginationLink>
            </PaginationItem>
        );
        }
    
        return (
            <div className="container" style={{marginBottom:50}}>
                    <div className="row" style={{marginBottom:90, marginTop:60,direction:sessionStorage.getItem("locale")==="ar"? "rtl":"ltr"}}>
                        <div className="col-md-6 col-12 d-flex">
                            <h1 className="myTitle align-self-center"><FormattedMessage id='myarticles'/></h1>
                           
                        </div>
                               <div className="col-md-6 col-12 d-flex justify-content-md-end justify-content-start" style={{marginBottom:20, marginTop:20}}>
                                <div className="btn-group flex-wrap flex-md-nowrap">
                                <button type="button" className="btn btnWhite active" ref="btnAllArticles" style={{width:'100%',marginTop:5}} onClick={this._allArticles} ><FormattedMessage id='Allarticles'/></button>
                                <button type="button" className="btn btnWhite" ref="btnAllSavedDrafts" style={{width:'100%',marginTop:5}} onClick={this._allSavedDrafts} ><FormattedMessage id='Saveddrafts'/></button>
                                <button type="button" className="btn btnWhite" ref="btnAllArticlesSubmitted" style={{width:'100%',marginTop:5}} onClick={this._allArticlesSubmitted} ><FormattedMessage id='Submitted'/></button>
                            
                             
                            </div>
                        </div>
                    </div>
                    <div className="row" style={{direction:sessionStorage.getItem("locale")==="ar"? "rtl":"ltr"}}>

                    {
                    this.state.loaded == false
                    ?
                    <div style={{marginTop:25}}>
                            <br/>

                        <div className="col-md-12  col-12" style={{marginLeft : 40}}> <img src='../../images/my_articles_loading_state.png' /> </div>
                   </div>
                    :

                        <div className="table-responsive">
                            <table className="table table-hover">
                                <thead>
                                    <tr>
                                        <th className="myText" style={{fontWeight:"bold"}}><FormattedMessage id='Thumbnail'/></th>    
                                        <th className="myText" style={{fontWeight:"bold"}}><FormattedMessage id='Title'/></th>    
                                        <th className="myText" style={{fontWeight:"bold"}}><FormattedMessage id='Category'/></th>    
                                        <th className="myText" style={{fontWeight:"bold"}}><FormattedMessage id='Status'/></th>    
                                        <th className="myText" style={{fontWeight:"bold"}}><FormattedMessage id='Action'/></th>    
                                    </tr>       
                                </thead>
                                <tbody>
                                    {this.state.ListTracks}
                                </tbody>
                            </table>
                        </div>
                    }
                    </div>
                   
            
                     <div className='paginationItem'>
                    <Pagination>
                        <PaginationItem
                            disabled={this.state.currentPage == 1 ? true:false}
                        >
                            <PaginationLink
                            previous
                            onClick={()=> this.handlePageClick(this.state.currentPage -1)}
                            />
                        </PaginationItem>
                            {pagination}
                        <PaginationItem
                            disabled={this.state.currentPage == this.state.Pages ? true:false}
                        >
                            <PaginationLink
                            onClick={()=>this.handlePageClick(this.state.currentPage+1)}
                            next
                            />
                        </PaginationItem>
                    </Pagination>
                </div>

                    <Modal
                        isOpen={this.state.modalIsOpen}
                        onRequestClose={this.closeModal}
                        style={customStyles}
                        contentLabel="Add media file"
                    >

                    <div className="container" style={{paddingBottom:30, paddingTop: 30}}>
                        <div className="row  d-flex justify-content-center" style={{marginBottom: 50}}>
                            <h1 className="mySubTitle"><FormattedMessage id='Areyousure'/></h1>
                        </div>
                        <div className="row">
                            <div className="col-md-6 d-flex justify-content-center">
                                <button type="button" 
                                        className="btn btnPink" 
                                        onClick={()=>{this.closeModal()}} 
                                        style={{width:200}} 
                                    ><FormattedMessage id='No'/></button>                                
                            </div>
                            <div className="col-md-6 d-flex justify-content-center">
                                <button type="button" 
                                        className="btn btnGreen" 
                                        onClick={()=>{this._delete()}} 
                                        style={{width:200}} 
                                    ><FormattedMessage id='Yes'/></button>
                            </div>
                        </div>
                    </div>

                    </Modal>

            </div>



        );
    }
}

export default withRouter(MyArticlesPage)
