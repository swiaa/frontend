/**
 * Author : Aymen FEZAI
 */
import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Route, Link, withRouter} from "react-router-dom";

//For popup messages
import toastr from 'toastr'
import 'toastr/build/toastr.min.css'
import {FormattedMessage} from 'react-intl';

toastr.options = {
    positionClass: 'toast-top-right',
    "closeButton": true,
    hideDuration: 300,
    timeOut: 3000
}
//End of toastr

class EditPassword extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            account: {}
        };
        this._changePassword = this
            ._changePassword
            .bind(this)
        this._cancel = this
            ._cancel
            .bind(this)
    }

    componentWillMount() {
        window.scroll({top: 0, left: 0, behavior: 'smooth'});

        this.setState({
            account: JSON.parse(localStorage.getItem('account'))
        })
    }

    handleChange(e) {
        e
            .target
            .classList
            .remove('invalidInput')
    }
    _cancel(event) {
        event.preventDefault();
        this
            .props
            .history
            .push('/');
    }
    _changePassword(event) {
        event.preventDefault();

        let account = this.state.account;

        let oldPwd = this.refs.oldPwd.value;
        let NewPwd = this.refs.NewPwd.value;
        let confPwd = this.refs.confPwd.value;

        if (oldPwd == "") {
            toastr.info("Please write your current password")
            this
                .refs
                .oldPwd
                .classList
                .add('invalidInput')
        } else {
            let options = {}
            options.method = 'GET'
            options.headers = {
                'x-access-token': account.token
            }

            fetch("http://35.204.178.205:4123/users/detail/" + account.id, options).then((res) => {
                if (res.ok) {
                    res
                        .json()
                        .then(res => {
                            if (res.hits.total > 0) {
                                if (res.hits.hits[0]._source.password != oldPwd) {
                                    toastr.info("Your current password is wrong")
                                    this
                                        .refs
                                        .oldPwd
                                        .classList
                                        .add('invalidInput')
                                } else {
                                    if (NewPwd == "" || NewPwd != confPwd) {
                                        toastr.info("Your new password doesn't much")
                                        this
                                            .refs
                                            .NewPwd
                                            .classList
                                            .add('invalidInput')
                                        this
                                            .refs
                                            .confPwd
                                            .classList
                                            .add('invalidInput')
                                    } else {
                                        let options2 = {}

                                        options2.method = 'POST'

                                        options2.headers = {
                                            'Accept': 'application/json',
                                            'Content-Type': 'application/json',
                                            'x-access-token': this.state.account.token
                                        }

                                        options2.body = JSON.stringify({id: this.state.account.id, password: NewPwd})

                                        fetch("http://35.204.178.205:4123/users/update", options2).then(res => {
                                            if (res.ok) {
                                                toastr.success("Done!")
                                                this
                                                    .refs
                                                    .oldPwd
                                                    .classList
                                                    .remove('invalidInput')
                                                this
                                                    .refs
                                                    .NewPwd
                                                    .classList
                                                    .remove('invalidInput')
                                                this
                                                    .refs
                                                    .confPwd
                                                    .classList
                                                    .remove('invalidInput')
                                                this.refs.oldPwd.value = ""
                                                this.refs.NewPwd.value = ""
                                                this.refs.confPwd.value = ""
                                            } else {
                                                console.error('Request failed with response ' + res.status + ' : ' + res.statusText);
                                            }
                                        }).catch(err => console.error(err))
                                    }
                                }
                            }
                        })
                        .catch(err => console.error(err))
                } else {
                    console.error('Request failed with response ' + res.status + ' : ' + res.statusText);
                }
            }).catch(err => console.error(err))
        }
    }

    render() {
        return (
            <div
                className="container"
                style={{
                fontFamily: "JF Flat Jozoor",
                direction: sessionStorage.getItem("locale") === "ar"
                    ? "rtl"
                    : "ltr"
            }}>

                <div
                    className="row"
                    style={{
                    fontFamily: "JF Flat Jozoor",
                    marginBottom: 10,
                    marginTop: 80
                }}>
                    <div className="col-md-12 col-12">
                        <h1
                            className="mySubTitle"
                            style={{
                            fontFamily: "JF Flat Jozoor",
                            textAlign: sessionStorage.getItem("locale") === "ar"
                                ? ('right')
                                : ('left')
                        }}><FormattedMessage id='password'/></h1>
                    </div>
                </div>

                <hr/>

                <div
                    className="col-md-10 col-12"
                    style={{
                    marginBottom: 80,
                    marginTop: 66
                }}>
                    <div className="form-group">
                        <FormattedMessage id='currentpassword'>
                            {placeholder => (<input
                                className="profileInputTxt"
                                type="password"
                                style={{
                                fontFamily: "JF Flat Jozoor",
                                textAlign: sessionStorage.getItem("locale") === "ar"
                                    ? ('right')
                                    : ('left')
                            }}
                                placeholder={placeholder}
                                onChange={this.handleChange}
                                ref="oldPwd"/>)}
                        </FormattedMessage>
                    </div>
                    <div className="form-group">

                        <FormattedMessage id='newpassword'>
                            {placeholder => (<input
                                className="profileInputTxt"
                                type="password"
                                style={{
                                fontFamily: "JF Flat Jozoor",
                                textAlign: sessionStorage.getItem("locale") === "ar"
                                    ? ('right')
                                    : ('left')
                            }}
                                placeholder={placeholder}
                                onChange={this.handleChange}
                                ref="NewPwd"/>)}
                        </FormattedMessage>
                    </div>
                    <div className="form-group">
                        <FormattedMessage id='confirmnewpassword'>
                            {placeholder => (<input
                                className="profileInputTxt"
                                type="password"
                                style={{
                                fontFamily: "JF Flat Jozoor",
                                textAlign: sessionStorage.getItem("locale") === "ar"
                                    ? ('right')
                                    : ('left')
                            }}
                                placeholder={placeholder}
                                onChange={this.handleChange}
                                ref="confPwd"/>)}
                        </FormattedMessage>
                    </div>
                </div>

                <hr/>

                <div
                    className="row"
                    style={{
                    marginTop: 20,
                    marginBottom: 50,
                    paddingLeft: 70,
                    paddingRight: 70
                }}>
                    <div
                        className="col-md-6 col-12 d-flex align-items-center"
                        style={{
                        marginTop: 10,
                        marginBottom: 10
                    }}>
                        <h3
                            className="mySmallTitle"
                            style={{
                            fontFamily: "JF Flat Jozoor",
                            textAlign: sessionStorage.getItem("locale") === "ar"
                                ? ('right')
                                : ('left')
                        }}>
                            <FormattedMessage id='savechanges'/></h3>
                    </div>
                    <div className="col-md-6 col-12">
                        <div className="float-right">
                            <button
                                className="btnWhite"
                                style={{
                                marginTop: 10,
                                marginBottom: 10,
                                marginRight: sessionStorage.getItem("locale") === "ar"
                                    ? 250
                                    : null
                            }}
                                onClick={this._cancel}><FormattedMessage id='cancel'/></button>
                            <button
                                className="btnGreen"
                                style={{
                                fontFamily: "JF Flat Jozoor",
                                marginTop: 10,
                                marginBottom: 10,
                                marginRight: sessionStorage.getItem("locale") === "ar"
                                    ? 10
                                    : null,
                                marginLeft: sessionStorage.getItem("locale") === "ar"
                                    ? null
                                    : 10
                            }}
                                onClick={this._changePassword}>
                                <FormattedMessage id='save'/>
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

export default withRouter(EditPassword)