/**
 * Author : Aymen FEZAI
 */

import React, {Component} from 'react';
import Test from '../Parts/Footer.jsx'
import LeadershipCard from './../Parts/LeadershipCard.jsx'
import TeamCard from './../Parts/TeamCard.jsx'
import toastr from 'toastr'
import 'toastr/build/toastr.min.css'
import {FormattedMessage} from 'react-intl';

import "../../images/aymen.jpg";
import "../../images/ahmed.jpg";
import "../../images/aya.jpg";

toastr.options = {
    positionClass: 'toast-top-right',
    "closeButton": true,
    hideDuration: 300,
    timeOut: 4000
}

class AboutusPage extends Component {
    componentWillMount() {
        window.scroll({top: 0, left: 0, behavior: 'smooth'});
    }

    render() {
        return (
            <div>
                <div
                    className="container"
                    style={{
                    paddingTop: 80,
                    fontFamily: "JF Flat Jozoor",
                    paddingBottom: 80,
                    direction: sessionStorage.getItem("locale") === "ar"
                        ? "rtl"
                        : "ltr"
                }}>
                    <div className="row">
                        <div className="col-md-10 offset-1 col-12">

                            <section
                                className="card card-lg"
                                style={{
                                paddingLeft: 20,
                                paddingRight: 20,
                                fontFamily: "JF Flat Jozoor"
                            }}>
                                <div className="card-body">
                                    <div className="d-flex justify-content-center">
                                        <h1
                                            className="myTitle"
                                            style={{
                                            marginTop: 20,
                                            color: "black"
                                        }}><FormattedMessage id='ourmession'/></h1>
                                    </div>
                                    <div className="d-flex justify-content-center">
                                        <p
                                            className="mb18Regular"
                                            style={{
                                            marginTop: 20,
                                            color: "black"
                                        }}>
                                            <FormattedMessage id='aboutparag'/>
                                        </p>
                                    </div>
                                    <div className="d-flex justify-content-center">
                                        <p
                                            className="mb20SemiBold"
                                            style={{
                                            marginTop: 20,
                                            color: "black"
                                        }}>
                                            <FormattedMessage id='about'/>
                                        </p>
                                    </div>
                                    <div
                                        className="row text-justify"
                                        style={{
                                        marginBottom: 20,
                                        color: "black"
                                    }}>
                                        <p
                                            className="mb14Regular text-justify"
                                            style={{
                                            marginTop: 20,
                                            color: "black"
                                        }}>
                                            <FormattedMessage id='aboutparag1'/></p>
                                        <p
                                            className="mb14Regular text-justify"
                                            style={{
                                            marginTop: 20,
                                            color: "black"
                                        }}>
                                            <FormattedMessage id='aboutparag2'/>
                                        </p>
                                    </div>
                                </div>
                            </section>

                            <section
                                className="card card-lg"
                                style={{
                                paddingLeft: 20,
                                paddingRight: 20,
                                marginTop: 40
                            }}>
                                <div className="card-body">
                                    <div className="d-flex justify-content-center">
                                        <h1
                                            className="myTitle"
                                            style={{
                                            marginTop: 20,
                                            color: "black"
                                        }}><FormattedMessage id='team'/></h1>
                                    </div>

                                    <div
                                        className="row"
                                        style={{
                                        marginTop: 20
                                    }}>
                                        <div className="col-md-4 col-12">
                                            <div
                                                className="card"
                                                style={{
                                                padding: 20,
                                                marginTop: 20,
                                                border: "none",
                                                direction: sessionStorage.getItem("locale") === "ar"
                                                    ? "rtl"
                                                    : "ltr"
                                            }}>
                                                <div className="d-flex justify-content-center">
                                                    <img
                                                        className="card-img-top rounded-circle img-fluid"
                                                        style={{
                                                        width: 150,
                                                        height: 150,
                                                        objectFit: "cover"
                                                    }}
                                                        src="../../images/aymen.jpg"
                                                        alt=""/>
                                                </div>
                                                <div className="d-flex justify-content-center">
                                                    <h5
                                                        class="card-title mySmallTitle"
                                                        style={{
                                                        marginTop: 20
                                                    }}>Aymen FEZAI</h5>
                                                </div>
                                                <div className="d-flex justify-content-center">
                                                    <h6
                                                        class="card-title mb14Regular text-center"
                                                        style={{
                                                        marginTop: 0
                                                    }}>Co-Founder & CDO/CTO</h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-4 col-12">
                                            <div
                                                className="card"
                                                style={{
                                                padding: 20,
                                                marginTop: 20,
                                                border: "none",
                                                direction: sessionStorage.getItem("locale") === "ar"
                                                    ? "rtl"
                                                    : "ltr"
                                            }}>
                                                <div className="d-flex justify-content-center">
                                                    <img
                                                        className="card-img-top rounded-circle img-fluid"
                                                        style={{
                                                        width: 150,
                                                        height: 150,
                                                        objectFit: "cover"
                                                    }}
                                                        src="../../images/ahmed.jpg"
                                                        alt=""/>
                                                </div>
                                                <div className="d-flex justify-content-center">
                                                    <h5
                                                        class="card-title mySmallTitle"
                                                        style={{
                                                        marginTop: 20
                                                    }}>Ahmed MZOUGHUI</h5>
                                                </div>
                                                <div className="d-flex justify-content-center">
                                                    <h6
                                                        class="card-title mb14Regular text-center"
                                                        style={{
                                                        marginTop: 0
                                                    }}>Co-Founder & CMO</h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-4 col-12">
                                            <div
                                                className="card"
                                                style={{
                                                padding: 20,
                                                marginTop: 20,
                                                border: "none",
                                                direction: sessionStorage.getItem("locale") === "ar"
                                                    ? "rtl"
                                                    : "ltr"
                                            }}>
                                                <div className="d-flex justify-content-center">
                                                    <img
                                                        className="card-img-top rounded-circle img-fluid"
                                                        style={{
                                                        width: 150,
                                                        height: 150,
                                                        objectFit: "cover"
                                                    }}
                                                        src="../../images/aya.jpg"
                                                        alt=""/>
                                                </div>
                                                <div className="d-flex justify-content-center">
                                                    <h5
                                                        class="card-title mySmallTitle"
                                                        style={{
                                                        marginTop: 20
                                                    }}>Aya AYARI</h5>
                                                </div>
                                                <div className="d-flex justify-content-center">
                                                    <h6
                                                        class="card-title mb14Regular text-center"
                                                        style={{
                                                        marginTop: 0
                                                    }}>Co-Founder & HR/CFO</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>

                <Test/>
            </div>
        );
    }
}

export default AboutusPage;