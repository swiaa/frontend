import React, {Component} from 'react';
import Test from './../Parts/testfooter.jsx'

import {Pagination, PaginationItem, PaginationLink} from 'reactstrap';

class ArticleLogPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            userName: "",
            userPic: "",

            notifications: [],

            tabIndex: 0,
            currentPageNotif: 1,
            pagesNotif: null
        }

        this._getNotifications = this
            ._getNotifications
            .bind(this);
        this._readNotification = this
            ._readNotification
            .bind(this);
    }

    componentDidMount() {
        window.scroll({top: 0, left: 0, behavior: 'smooth'});

        if (sessionStorage.getItem('isloggedin') == "true") {
            this.setState({isloggedin: true})
            this._getNotifications(this.state.currentPageNotif)
        } else {
            this.setState({isloggedin: false})
        }

        // this.interval = setInterval(() => {
        if (sessionStorage.getItem('isloggedin') == "true") {
            this.setState({isloggedin: true})
            this._getNotifications()
        } else {
            this.setState({isloggedin: false})
        }
        // }, 5000)
    }

    _getNotifications(pageIndex) {
        let options = {};
        let link = "";

        options.headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'x-access-token': JSON
                .parse(localStorage.account)
                .token,
            'from': (pageIndex - 1) * 10,
            'size': 10,
            'article_id': this.props.location.state.article_id
        };
        link = "http://35.204.178.205:4123/notification/getArticleLogs";

        options.method = 'GET';

        fetch(link, options).then(res => {
            if (res.ok) {
                res
                    .json()
                    .then(res => {
                        if (res.result && res.result.hits && res.result.hits.hits) {
                            let pagesNotif = Math.ceil(res.result.hits.total / 12); //Pagination Image
                            this.setState({pagesNotif, currentPageNotif: pageIndex}); //Pagination Image

                            this.setState({notifications: res.result.hits.hits})
                        }
                    })
            } else {
                console.error('Network request failed with response ' + res.status + ' : ' + res.statusText);
            }
        }).catch(err => console.error(err))
    }

    _readNotification(notification_id, article_id) {
        let options = {}

        options.method = 'POST'

        options.headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'x-access-token': JSON
                .parse(localStorage.account)
                .token
        }

        options.body = JSON.stringify({notification_id})

        fetch("http://35.204.178.205:4123/notification/readNotification", options).then(res => {
            if (res.ok) {
                res
                    .json()
                    .then(res => {
                        this._getNotifications()
                        this
                            .props
                            .history
                            .push('/article/' + article_id);
                    })
            } else {
                console.error('Network request failed with response ' + res.status + ' : ' + res.statusText);
            }
        }).catch(err => console.error(err))
    }

    handlePageClickNotif(pageIndex) {
        window.scroll({top: 0, left: 0, behavior: 'smooth'});
        this.setState({currentPageDocument: pageIndex, loaded: false});
        this._getNotifications(pageIndex);
    }

    render() {
        let pagination1 = [];
        for (let pageIndex = 1; pageIndex <= this.state.pagesNotif; pageIndex++) {
            pagination1.push(
                <PaginationItem
                    key={pageIndex}
                    active={this.state.currentPageNotif == pageIndex
                    ? true
                    : false}>
                    <PaginationLink onClick={() => this.handlePageClickNotif(pageIndex)}>
                        {pageIndex}
                    </PaginationLink>
                </PaginationItem>
            );
        }

        let notificationsItems = [];

        if (this.state.notifications.length > 0) {
            for (let i = 0; i < this.state.notifications.length; i++) {
                let divClass = "";
                let cardClass = "";

                if (this.state.notifications[i]._source.color == "green") {
                    if (this.state.notifications[i]._source.viewed) {
                        divClass = "card border-success mb-3"
                    } else {
                        divClass = "card mySuccessCard border-success mb-3"
                    }
                    cardClass = "card-body text-success"
                } else {
                    if (this.state.notifications[i]._source.viewed) {
                        divClass = "card border-danger mb-3"
                    } else {
                        divClass = "card myDangerCard border-danger mb-3"
                    }
                    cardClass = "card-body text-danger"
                }

                notificationsItems.push(
                    <div
                        key={i}
                        className="row"
                        style={{
                        marginBottom: 20,
                        marginTop: 20,
                        cursor: "pointer"
                    }}
                        onClick={() => this._readNotification(this.state.notifications[i]._id, this.state.notifications[i]._source.article_id)}>
                        <div
                            className={divClass}
                            style={{
                            width: "100%",
                            float: "right"
                        }}>
                            <div className={cardClass}>
                                <div
                                    style={{
                                    float: "left"
                                }}>
                                    {(this.state.notifications[i]._source.expert_profilepic == "")
                                        ? (<img
                                            src='../../images/defaultAvatar.png'
                                            className="img-responsive rounded-circle align-self-center"
                                            style={{
                                            width: 50,
                                            height: 50,
                                            marginRight: 20
                                        }}/>)
                                        : (<img
                                            src={this.state.notifications[i]._source.expert_profilepic}
                                            className="img-responsive rounded-circle align-self-center"
                                            style={{
                                            width: 50,
                                            height: 50,
                                            marginRight: 20
                                        }}/>)
                                    }

                                </div>
                                <div>
                                    <p
                                        className="card-text"
                                        style={{
                                        fontWeight: "bold",
                                        marginBottom: "10px"
                                    }}>{this.state.notifications[i]._source.expert_name}
                                        :
                                    </p>
                                    <p
                                        className="card-text"
                                        style={{
                                        marginBottom: "10px"
                                    }}>{this.state.notifications[i]._source.messageHeader}</p>
                                    <p className="card-text">{this.state.notifications[i]._source.messageBody}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                )
            }
        }

        return (
            <div>

                <div className="container">
                    <div
                        className="row"
                        style={{
                        marginBottom: 30,
                        marginTop: 80
                    }}>
                        <div className="col-md-12 col-12">
                            <h1 className="mySubTitle">Article "{this.props.location.state.article_name}" Logs</h1>
                        </div>
                    </div>
                    <hr
                        style={{
                        marginBottom: 20,
                        marginTop: 20
                    }}/>

                    <div className="container">

                        {notificationsItems}

                    </div>

                    <div
                        className="row d-flex justify-content-center"
                        style={{
                        paddingBottom: 50
                    }}>
                        <Pagination>
                            <PaginationItem
                                disabled={this.state.currentPageNotif == 1
                                ? true
                                : false}>
                                <PaginationLink
                                    previous
                                    onClick={() => this.handlePageClickNotif(this.state.currentPageNotif - 1)}/>
                            </PaginationItem>
                            {pagination1}
                            <PaginationItem
                                disabled={this.state.currentPageNotif == this.state.pagesNotif
                                ? true
                                : false}>
                                <PaginationLink
                                    onClick={() => this.handlePageClickNotif(this.state.currentPageNotif + 1)}
                                    next/>
                            </PaginationItem>
                        </Pagination>
                    </div>
                </div>

                <Test/>
            </div>
        );
    }
}

export default ArticleLogPage;
