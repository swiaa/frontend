/**
 * Author : Aymen FEZAI
 */

import React, {Component} from 'react';

import Test from '../Parts/Footer.jsx'
import {FormattedMessage} from 'react-intl';

class ContactPage extends Component {
    componentWillMount() {
        window.scroll({top: 0, left: 0, behavior: 'smooth'});
    }

    render() {
        return (
            <div>
                <div
                    className="container"
                    style={{
                    fontFamily: "JF Flat Jozoor",
                    paddingTop: 80,
                    paddingBottom: 80,
                    direction: sessionStorage.getItem("locale") === "ar"
                        ? "rtl"
                        : "ltr"
                }}>
                    <div className="row">
                        <div className="col-md-12 col-12">
                            <div
                                className="row"
                                style={{
                                marginBottom: 20
                            }}>
                                <div
                                    className="col-md-8 offset-2 col-12"
                                    style={{
                                    padding: 20
                                }}>
                                    <div className="card">
                                        <div className="card-header">
                                            <div className="row d-flex justify-content-center">
                                                <h1
                                                    className="myTitle"
                                                    style={{
                                                    fontFamily: "JF Flat Jozoor"
                                                }}><FormattedMessage id='contactUs'/></h1>
                                            </div>
                                            <div
                                                className="row d-flex justify-content-center"
                                                style={{
                                                fontFamily: "JF Flat Jozoor",
                                                padding: 20
                                            }}>
                                                <p className="mb18Regular">
                                                    <FormattedMessage id='contactUsparag'/>
                                                </p>
                                            </div>
                                        </div>
                                        <div className="card-body">
                                            <form action="">
                                                <div
                                                    className="col-md-12 col-12"
                                                    style={{
                                                    marginBottom: 20
                                                }}>
                                                    <FormattedMessage id='yourname'>
                                                        {placeholder => (<input
                                                            type="text"
                                                            className="inputSelect myInputText"
                                                            style={{
                                                            fontFamily: "JF Flat Jozoor",
                                                            height: 60,
                                                            textAlign: sessionStorage.getItem("locale") === "ar"
                                                                ? ('right')
                                                                : ('left')
                                                        }}
                                                            placeholder={placeholder}
                                                            ref="name"/>)}
                                                    </FormattedMessage>
                                                </div>
                                                <div
                                                    className="col-md-12 col-12"
                                                    style={{
                                                    marginBottom: 20,
                                                    fontFamily: "JF Flat Jozoor"
                                                }}>
                                                    <FormattedMessage id='youremail'>
                                                        {placeholder => (<input
                                                            type="text"
                                                            className="inputSelect myInputText"
                                                            style={{
                                                            fontFamily: "JF Flat Jozoor",
                                                            height: 60,
                                                            textAlign: sessionStorage.getItem("locale") === "ar"
                                                                ? ('right')
                                                                : ('left')
                                                        }}
                                                            placeholder={placeholder}
                                                            ref="email"/>)}
                                                    </FormattedMessage>
                                                </div>
                                                <div
                                                    className="col-md-12 col-12"
                                                    style={{
                                                    marginBottom: 20
                                                }}>

                                                    <FormattedMessage id='subject'>
                                                        {placeholder => (<input
                                                            type="text"
                                                            className="inputSelect myInputText"
                                                            style={{
                                                            fontFamily: "JF Flat Jozoor",
                                                            height: 60,
                                                            textAlign: sessionStorage.getItem("locale") === "ar"
                                                                ? ('right')
                                                                : ('left')
                                                        }}
                                                            placeholder={placeholder}
                                                            ref="subject"/>)}
                                                    </FormattedMessage>
                                                </div>
                                                <div
                                                    className="col-md-12 col-12"
                                                    style={{
                                                    marginBottom: 20
                                                }}>
                                                    <FormattedMessage id='yourmessage'>
                                                        {placeholder => (
                                                            <textarea
                                                                className="inputSelect myInputText"
                                                                style={{
                                                                fontFamily: "JF Flat Jozoor",
                                                                height: 180,
                                                                textAlign: sessionStorage.getItem("locale") === "ar"
                                                                    ? ('right')
                                                                    : ('left')
                                                            }}
                                                                placeholder={placeholder}
                                                                ref="message"></textarea>
                                                        )}
                                                    </FormattedMessage>
                                                </div>
                                                <div className="row">
                                                    <div
                                                        className="col-12 d-flex justify-content-center"
                                                        style={{
                                                        fontFamily: "JF Flat Jozoor"
                                                    }}>
                                                        <button className="btnGreen">
                                                            <FormattedMessage id='send'/></button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <Test/>
            </div>
        );
    }
}

export default ContactPage;