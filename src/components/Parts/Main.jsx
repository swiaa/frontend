import React from 'react'
import {Switch, Route} from 'react-router-dom'
import {Provider} from 'react-redux';
import {combineReducers, createStore, compose, applyMiddleware} from 'redux';
import {sessionService, sessionReducer} from 'redux-react-session';
import thunkMiddleware from 'redux-thunk';
import Job from './../Screens/Job.jsx';
import ExplorePage from './../Screens/ExplorePage.jsx';
import Home from './../Screens/Home.jsx';
import ProfilePage from "./../Screens/ProfilePage.jsx"
import EditProfilePage from "./../Screens/EditProfilePage.jsx"
import EditPassword from "./../Screens/EditPassword.jsx"
import ActivationPage from "./../Screens/ActivationPage.jsx"
import ContactPage from "./../Screens/ContactPage.jsx"
import AboutusPage from "./../Screens/AboutusPage.jsx"
import NotificationPage from "./../Screens/NotificationPage.jsx"
import NewJobPage from './../Screens/NewJobPage.jsx';

// Add the sessionReducer
const reducer = combineReducers({session: sessionReducer});

const store = createStore(reducer, undefined, compose(applyMiddleware(thunkMiddleware)));

// Init the session service
sessionService.initSessionService(store, {driver: 'COOKIES'});

sessionService
    .initSessionService(store, {})
    .then(() => console.log('Redux React Session is ready and a session was refreshed from your storage'))
    .catch(() => console.log('Redux React Session is ready and there is no session in your storage'));

// The Main component renders one of the three provided Routes (provided that
// one matches). Both the /roster and /schedule routes will match any pathname
// that starts with /roster or /schedule. The / route will only match when the
// pathname is exactly the string "/"
const Main = () => (
    <main>

        <Provider store={store}>
            <Switch>

                <Route exact path='/' name="Home Page" render={props => (<Home {...props}/>)}/>
                <Route
                    path='/profile'
                    name="Profile"
                    render={props => (<ProfilePage {...props}/>)}/>
                <Route
                    path='/editProfile'
                    name="Edit Profile"
                    render={props => (<EditProfilePage {...props}/>)}/>
                <Route
                    path='/editPassword'
                    name="Edit Profile"
                    render={props => (<EditPassword {...props}/>)}/>
                <Route
                    path='/Activation/id=:id'
                    name="Activation  Page"
                    render={props => (<ActivationPage {...props}/>)}/>
                <Route
                    path='/Contact'
                    name="Contact  Page"
                    render={props => (<ContactPage {...props}/>)}/>                
                <Route
                    path='/AboutUs'
                    name="About us  Page"
                    render={props => (<AboutusPage {...props}/>)}/>
                <Route
                    path='/Notification'
                    name="Notification  Page"
                    render={props => (<NotificationPage {...props}/>)}/>
                <Route
                    path='/newJob'
                    name="New Job  Page"
                    render={props => (<NewJobPage {...props}/>)}/>
                <Route
                    path='/job/:number'
                    name="Job Page"
                    render={props => (<Job {...props}/>)}/>
                <Route
                    path='/explore'
                    name="Explore Page"
                    render={props => (<ExplorePage {...props}/>)}/>
            </Switch>
        </Provider>

    </main>
)

export default Main
