import React from 'react'

import {Twitter} from '../../images/twitter.svg'
import {facebook} from '../../images/facebook.svg'
import {instagram} from '../../images/instagram.svg'
import {FormattedMessage} from 'react-intl';

const textStyle = {
    fontFamily: "JF Flat Jozoor",
    fontSize: 16,
    fontStyle: "normal",
    textAlign: "left",
    color: "#F0E40F",
    marginTop: 10,
    textAlign: "center"
};

export default class Test extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        return (
            <div
                style={{
                direction: sessionStorage.getItem("locale") === "ar"
                    ? "rtl"
                    : "ltr"
            }}>

                <div
                    style={{
                    backgroundColor: '#000',
                    paddingTop: 10
                }}
                    className="desktopfooter">
                    <div class="container">
                        <div
                            class="row align-items-center justify-content-center"
                            style={{
                            marginTop: 49
                        }}>
                            <div class="col text-center">
                                <p style={textStyle}><FormattedMessage id='allrigth'/></p>
                            </div>
                        </div>

                        <div
                            className="row align-items-center justify-content-center"
                            style={{
                            marginTop: 30,
                        }}>

                            <div
                                className="col text-center"
                                style={{
                                marginBottom: 30
                            }}>
                                <embed
                                    src="../../images/facebook.svg"
                                    style={{
                                    width: 26,
                                    height: 26,
                                    marginRight: 26
                                }}/>
                                <embed
                                    src="../../images/twitter.svg"
                                    style={{
                                    width: 26,
                                    height: 26,
                                    marginRight: 26
                                }}/>
                                <embed
                                    src="../../images/instagram.svg"
                                    style={{
                                    width: 26,
                                    height: 26
                                }}/>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )

    }

}
