import React from 'react'
import { BrowserRouter as Router, Route, Link ,withRouter} from "react-router-dom";
import FontAwesome from 'react-fontawesome'
import '../../images/demenagement.jpg'

const paragraph = {
    fontFamily: "JF Flat Jozoor",
    fontSize: 10.5,
    fontWeight: "300",
    fontStyle: "normal",
    letterSpacing: 0.11,
    textAlign: "justify",
    color: "#3c3d41",
    marginBottom : 18
};

const byAuthorName = {
    fontFamily: "JF Flat Jozoor",
    fontSize: 11,
    fontWeight: "normal",
    fontStyle: "normal",
    letterSpacing: 0.07,
    textAlign: "left",
    color: "#838d8f",
    textOverflow: "ellipsis",
    overflow: "hidden",
    whiteSpace: "nowrap"
};

class TodayCollectionCard extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
        }  

        this._navigate = this._navigate.bind(this);
    }

    componentWillMount(){
        
    }

    _navigate(var1){
        this.props.history.push('/job/'+var1) ;
    }

    render() {

        return (
            <div>
                <div  onClick={() => {this._navigate(121)}} style={{ width :250,height:305,marginTop:30,marginBottom:30,marginRight:0,marginLeft:0}} className="cardStyle">
                    <div onClick={() => {this._navigate(121)}} style={{backgroundImage: 'url(../../images/demenagement.jpg)'}} class="backgroundcard withShadow">
                        <div className="categoryslider" align="center" style={{width:120}}>
                            <br></br>
                            <p className="txtcategory" >20 DT</p>
                        </div>
                    </div>

                    <div class="container cardcontainer withShadow" style={{backgroundColor:'white',height:145}}>
                        <h1 className="myCard">Title</h1>
                        <div style={{height: 61 , overflow: 'hidden'}}>
                            <p style={paragraph}>
                                Cupidatat labore esse veniam voluptate minim excepteur tempor cupidatat amet reprehenderit tempor proident eu tempor. Deserunt tempor sunt enim cupidatat ex ut elit non. Labore qui laboris qui ea velit aliqua labore laborum excepteur amet Lorem nisi reprehenderit. Proident veniam est velit minim anim do consectetur dolore ut nostrud. Laborum officia eiusmod officia magna enim minim veniam adipisicing. Anim veniam sint eu labore exercitation anim quis dolore.
                            </p>
                        </div>

                        <hr style={{marginBottom:12}} />
                        <div>
                            <p style={byAuthorName} class="byauthor"><span style={{fontWeight:"bold"}}>Author : </span>Foulen</p>                                                    
                        </div>
                    </div>

                </div>                
            </div>
        )

    }

}

export default withRouter(TodayCollectionCard)
