import React from 'react'
import {loadimage} from '../../images/loading_state_2.png'

import TodayCollectionCard from './TodayCollectionCard.jsx'
import {BrowserRouter as Router, Route, Link, withRouter} from "react-router-dom";

var Loader = require('react-loader');
import {FormattedMessage} from 'react-intl';

const todaysCollection = {
    fontFamily: "JF Flat Jozoor",
    fontSize: 38,
    fontStyle: "normal",
    textAlign: "left",
    color: "#000",
    marginTop: 40
};

const seeAll = {
    fontFamily: "JF Flat Jozoor",
    fontSize: 15,
    color: "#E7B34B",
    textdecoration: "underline",
    cursor: "pointer",
    textAlign: sessionStorage.getItem("locale") === "ar"
        ? ('left')
        : ('right')
};

class TodayCollection extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            ArrayList: [],
            loaded: false
        }
    }

    componentDidMount() {

        this.setState({loaded: false});

        let options = {};
        options.body = JSON.stringify({paginationStart: 0, paginationEnd: 9});

        options.headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'x-access-token': 'gd654dg@fuyffd5'
        };

        options.method = 'POST';

        // TODO change URL

        fetch("http://35.204.178.205:4123/categorization/getTopTen", options).then((response) => response.json()).then((response) => {
            if (response != 'null') {
                this.setState({loaded: true, home: false});

                var mainlist = response.hits;
                this.setState({total: response.total});

                var listItems = mainlist.map((item) => {
                    return (
                        <div key={item._id} class="col-xs-12 col-sm-6 col-lg-4 d-flex justify-content-center">
                            <TodayCollectionCard data={item}/>
                        </div>
                    );
                });

                this.setState({ArrayList: listItems});
            } else {
                console.log(response);
            }
        }).catch((error) => {
            console.error(error);
        });
    }

    render() {

        return (
            <div
                style={{
                backgroundColor: '#fbfbfb',
                paddingBottom: 30
            }}>
                <div class="container">

                    <div className="container">
                        <div
                            class="row"
                            style={{
                            marginBottom: 20,
                            direction: sessionStorage.getItem("locale") === "ar"
                                ? "rtl"
                                : "ltr"
                        }}>
                            <h1 style={todaysCollection}><FormattedMessage id='todaycollection'/></h1>
                            <div
                                class="col"
                                style={{
                                paddingTop: 65
                            }}>
                                <a href="/explore"><p style={seeAll}><FormattedMessage id='seeall'/></p></a>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        {this.state.loaded
                            ? this.state.ArrayList
                            : <img src='../../images/loading_state_2.png' width="100%"/>
}

                    </div>

                </div>
            </div>
        )

    }

}

export default withRouter(TodayCollection)
