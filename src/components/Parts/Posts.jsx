import React from 'react'
import {Background} from '../../images/headerImage.jpg';
import {SearchIcon} from '../../images/SearchIcon.png';
;
import {landscape} from '../../images/slider.jpg';

import Article from '../Screens/Job.jsx'

import {imgIcon} from '../../images/imgIcon.png'
import {vidIcon} from '../../images/vidIcon.png'
import {articleIcon} from '../../images/articleIcon.png'

import {img1} from '../../images/img1.png'
import {img2} from '../../images/img2.png'
import {img3} from '../../images/img3.png'
import {transparent} from '../../images/transparent.png'

import {BrowserRouter as Router, Route, Link} from "react-router-dom";

export default class Posts extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        return (

            <div style={{
                height: 460
            }}>
                <div className="container-fluid">
                    <br></br>
                    <div
                        className="categoryslider"
                        align="center"
                        style={{
                        width: 201
                    }}>
                        <br></br>
                        <Link to="/article">
                            <p
                                className="txtcategory"
                                style={{
                                width: 201,
                                height: 29
                            }}>POSTS OF THE DAY</p>
                        </Link>
                    </div>

                    <div
                        className="row"
                        style={{
                        marginTop: 20
                    }}>

                        <div
                            className="col posts"
                            style={{
                            height: 401,
                            backgroundImage: 'url(' +
                                    '../images/img1.png' +
                                    ')'
                        }}>
                            <img src="../images/imgIcon.png" className="iconpost"/>
                            <div className="divbottomtext">
                                <p className="bottomtext col-md-9">
                                    key to sharing your Al Madinah memories. A platform through which you can learn
                                    exciting things about Al Madinah from the memories of others.</p>
                            </div>

                        </div>

                        <div
                            className="col vidContain"
                            style={{
                            height: 401
                        }}>
                            <div class='vid'>
                                <video
                                    width="100%"
                                    height="401"
                                    poster="../images/transparent.png"
                                    style={{
                                    backgroundImage: 'url(' +
                                            '../images/img2.png' +
                                            ')'
                                }}
                                    controls>
                                    <source src="http://daydreamersih.com/FEB2018.mp4" type="video/mp4"/>
                                </video>
                            </div>
                            <img src="../images/vidIcon.png" className="iconpost"/>
                            <div className="divbottomtext">
                                <p className="bottomtext col-md-9">

                                    key to sharing your Al Madinah memories. A platform through which you can learn
                                    exciting things about Al Madinah from the memories of others.
                                </p>
                            </div>
                        </div>

                        <div
                            className="col posts"
                            style={{
                            height: 401,
                            backgroundImage: 'url(' +
                                    '../images/img3.png' +
                                    ')'
                        }}>
                            <div className="layer">
                                <img src="../images/articleIcon.png" className="iconpost"/>
                                <div className="container paragraphcontainer">
                                    <p className="paragraphpost">key to sharing your Al Madinah memories. A platform
                                        through which you can learn exciting things about Al Madinah from the memories
                                        of others. key to sharing your Al Madinah memories. A platform through which you
                                        can learn exciting things about Al Madinah from the memories of others. key to
                                        sharing your Al Madinah memories. A platform through which you can learn
                                        exciting things about Al Madinah from the memories of others. key to sharing
                                        your Al Madinah memories. A platform through which you can learn exciting things
                                        about Al Madinah from the memories of others.</p>
                                    <p className="paragraphpostreadmore">READ MORE</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        )

    }

}