import React from 'react'
import '../../images/swiaa_white.png';
import '../../images/ar.png';
import '../../images/en.png';
import '../../images/MenuMobileShowGroup.svg'
import '../../images/MenuMobileShowGroup.png'
import '../../images/MenuMobileCloseGroup.svg'
import '../../images/MenuMobileCloseGroup.png'
import '../../images/personIcon.png';
import '../../images/defaultAvatar.png'

import '../../sass/layout/_profilecss.scss'
import FontAwesome from 'react-fontawesome'
import {BrowserRouter as Router, Route, Link, withRouter} from "react-router-dom";
import LoginPage from './../Modals/LoginPage.jsx'
import SignupPage from './../Modals/SignupPage.jsx'
import {FormattedMessage, injectIntl} from 'react-intl';

//For popup messages
import toastr from 'toastr'
import 'toastr/build/toastr.min.css'

toastr.options = {
    positionClass: 'toast-top-right',
    "closeButton": true,
    hideDuration: 300,
    timeOut: 4000
}
//End of toastr

const elements = {
    fontFamily: "JF Flat Jozoor",
    fontSize: 18,
    fontWeight: "normal",
    fontStyle: "normal",
    letterSpacing: 0.13,
    textAlign: "left",
    color: "#838d8f",
    marginTop: 20
};

class Navtop extends React.Component {
    Logo
    constructor(props) {
        super(props)

        this.state = {
            menumobile: false,
            loginPageIsOpen: false,
            signupPageIsOpen: false,
            proReqPageIsOpen: false,
            professionlPageIsOpen: false, ///modify
            showMenu: false, //DropDown Menu
            showNotificationMenu: false, //Show notification Menu
            isloggedin: false,
            isProfessional: false,
            firstruntest: false,
            account: '',

            nbNotifications: 5,
            notifications: [],
            testclickbtn: false,
            testclickbtngetstarted: false
        };

        this.openmenu = this
            .openmenu
            .bind(this);
        this.openLoginModal = this
            .openLoginModal
            .bind(this);
        this.openSignupModal = this
            .openSignupModal
            .bind(this);
        this.openProfessionalUserModal = this
            .openProfessionalUserModal
            .bind(this); ////modify
        this.openProReqModal = this
            .openProReqModal
            .bind(this);
        this.closeModal = this
            .closeModal
            .bind(this);
        this.showMenu = this
            .showMenu
            .bind(this); //DropDown Menu
        this.closeMenu = this
            .closeMenu
            .bind(this); //DropDown Menu
        this.showNotificationMenu = this
            .showNotificationMenu
            .bind(this); //DropDown Notification Menu
        this.login = this
            .login
            .bind(this); //Login Methode
        // this.signup = this.signup.bind(this); //signup Methode
        this.logout = this
            .logout
            .bind(this); //logout Methode
        this.goHome = this
            .goHome
            .bind(this); 
        this.changeLangueAr = this
            .changeLangueAr
            .bind(this);
        this.changeLangueEn = this
            .changeLangueEn
            .bind(this);
        this._openNewJob = this
            ._openNewJob
            .bind(this);
        this._getNotifications = this
            ._getNotifications
            .bind(this);
        this._readNotification = this
            ._readNotification
            .bind(this);
        this.openprofile = this
            .openprofile
            .bind(this);
        this.createjob = this
            .createjob
            .bind(this);

    }

    render() {
        let notificationsItems = [];
            for (let i = 0; i < 5; i++) {
                let divClass = "";
                let cardClass = "";
                if (i%2 == 0) {
                    divClass = "card mySuccessCard border-success mb-3"
                    cardClass = "card-body text-success"
                } else {
                    divClass = "card myDangerCard border-danger mb-3"
                    cardClass = "card-body text-danger"
                }
                notificationsItems.push(
                    <div
                        key={i}
                        className={divClass}
                        style={{
                        maxWidth: "400px"
                    }}
                        onClick={() => this._readNotification(101)}>
                        <div className={cardClass}>
                            <p className="card-text">
                                Incididunt excepteur in in sint ad eiusmod nulla labore 
                            </p>
                        </div>
                    </div>
                )
        }

        return (
            <div
                style={{
                direction: sessionStorage.getItem("locale") === "ar"
                    ? "rtl"
                    : "ltr"
            }}>
                <LoginPage
                    data={this.state}
                    closeModal={this.closeModal}
                    loginFun={this.login}/>
                <SignupPage
                    data={this.state}
                    closeModal={this.closeModal}
                    signupFun={this.signup}/>

                <div className="navtop">{/* Menu ----------------------------------------------------------------------------------- */}
                    <div
                        className="container-fluid"
                        style={{
                        paddingRight: 50
                    }}>
                        <div className="row">
                            <div className="col-md-4">

                                <img
                                    onClick={() => {
                                    this.goHome()
                                }}
                                    src="../images/swiaa_white.png"
                                    style={{
                                    height: 70,
                                    width: 50,
                                    marginTop: 10,
                                    marginLeft: sessionStorage.getItem("locale") == "ar"
                                        ? 350
                                        : 25
                                }}/> {this.state.menumobile
                                    ? <div
                                            onClick={() => {
                                            this.closemenu()
                                        }}>
                                            <img
                                                src="../images/MenuMobileCloseGroup.svg"
                                                className="iconmobilemenu mobileitem"/>
                                        </div>
                                    : <div
                                        onClick={() => {
                                        this.openmenu()
                                    }}>
                                        <img
                                            src="../images/MenuMobileShowGroup.svg"
                                            className="iconmobilemenu mobileitem"/>
                                    </div>
}

                            </div>

                            {this.state.isloggedin
                                ? <div className="col-md-8 d-flex justify-content-end">

                                        <div className="p-2">
                                            <Link
                                                to={sessionStorage.getItem("locale") == "ar"
                                                ? '/?locale=ar'
                                                : '/'}
                                                onClick={true}>
                                                <p className="navtop-item">Home</p>
                                            </Link>
                                        </div>

                                        <div className="p-2">
                                            <Link
                                                to={sessionStorage.getItem("locale") == "ar"
                                                ? '/explore/?locale=ar'
                                                : '/explore'}
                                                onClick={true}>
                                                <p className="navtop-item"><FormattedMessage id='explore'/></p>
                                            </Link>
                                        </div>

                                        <div className="p-2">
                                            <Link
                                                to={sessionStorage.getItem("locale") == "ar"
                                                ? '/contact/?locale=ar'
                                                : '/contact'}
                                                onClick={true}>
                                                <p className="navtop-item"><FormattedMessage id='contact'/></p>
                                            </Link>
                                        </div>
                                        <div className="p-2">
                                            <Link
                                                to={sessionStorage.getItem("locale") == "ar"
                                                ? '/aboutus/?locale=ar'
                                                : '/aboutus'}
                                                onClick={true}>
                                                <p className="navtop-item"><FormattedMessage id='aboutus'/></p>
                                            </Link>
                                        </div>
                                        <div
                                            className=" circlebody row "
                                            style={{
                                            marginTop: 20,
                                            marginLeft: 10,
                                            marginRight: 10
                                        }}
                                            onClick={this.openprofile}>
                                            <h1
                                                className="circle"
                                                style={{
                                                marginTop: 3,
                                                marginLeft: 3
                                            }}>{localStorage
                                                    .getItem('nameuser')
                                                    .substr(0, 1)}</h1>
                                            <h1
                                                style={{
                                                marginTop: 10,
                                                paddingLeft: 5,
                                                paddingRight: 5
                                            }}>{localStorage.getItem('nameuser')}</h1>
                                        </div>

                                        <div
                                            className="row"
                                            onClick={this.showNotificationMenu}
                                            style={{
                                            position: "relative"
                                        }}>
                                            {(this.state.nbNotifications > 0)
                                                ? (
                                                    <div
                                                        className="notification rounded-circle withShadow d-flex justify-content-center align-items-center">
                                                        5
                                                    </div>
                                                )
                                                : (
                                                    null
                                                )
}

                                            <div
                                                className="circlebodymenu"
                                                style={{
                                                marginTop: 18,
                                                marginLeft: 15,
                                                marginRight: 15
                                            }}>
                                                <FontAwesome name='bell stylefont' size='2x'/>
                                            </div>
                                        </div>

                                        <div
                                            className=" circlemenu row "
                                            style={{
                                            marginTop: 20,
                                            marginLeft: 10,
                                            marginRight: 10
                                        }}
                                            onClick={this.showMenu}>
                                            <FontAwesome
                                                className='fa fa-ellipsis-h'
                                                name='ellipsis'
                                                size='2x'
                                                style={{
                                                cursor: "pointer",
                                                padding: 5
                                            }}/>
                                        </div>

                                        <div
                                            className="row "
                                            style={{
                                            marginTop: 20,
                                            marginLeft: 5,
                                            marginRight: 10
                                        }}>
                                            {(sessionStorage.getItem("locale") === "ar"
                                                ? (<img
                                                    onClick={() => {
                                                    this.changeLangueEn()
                                                }}
                                                    src="../../images/en.png"
                                                    style={{
                                                    height: 25,
                                                    width: 25,
                                                    marginTop: 5,
                                                    marginRight: 10
                                                }}/>)
                                                : (<img
                                                    onClick={() => {
                                                    this.changeLangueAr()
                                                }}
                                                    src="../../images/ar.png"
                                                    style={{
                                                    height: 25,
                                                    width: 25,
                                                    marginTop: 5,
                                                    marginLeft: 10
                                                }}/>))
}
                                        </div>
                                        <div
                                            style={{
                                            marginTop: 30
                                        }}>
                                            {(sessionStorage.getItem("locale") === "ar"
                                                ? (
                                                    <h1
                                                        className="langueEN"
                                                        onClick={() => {
                                                        this.changeLangueEn()
                                                    }}>English</h1>
                                                )
                                                : (
                                                    <h1
                                                        className="langueAR"
                                                        onClick={() => {
                                                        this.changeLangueAr()
                                                    }}>العربية</h1>
                                                ))
}
                                        </div>
                                        <div></div>
                                    </div>
                                : <div className="col-md-8 d-flex justify-content-end">
                                    <div
                                        className="p-2"
                                        style={{
                                        marginTop: 3
                                    }}>
                                        <Link
                                            to=
                                            {sessionStorage.getItem("locale")=="ar" ?'/explore/?locale=ar' :'/explore'}
                                            onClick={true}>
                                            <p className="navtop-item"><FormattedMessage id='explore'/></p>
                                        </Link>
                                    </div>
                                    <div
                                        className="p-2"
                                        style={{
                                        marginTop: 3
                                    }}>
                                        <Link
                                            to={sessionStorage.getItem("locale") == "ar"
                                            ? '/contact/?locale=ar'
                                            : '/contact'}
                                            onClick={true}>
                                            <p className="navtop-item"><FormattedMessage id='contact'/></p>
                                        </Link>
                                    </div>
                                    <div
                                        className="p-2"
                                        style={{
                                        marginTop: 3
                                    }}>
                                        <Link
                                            to={sessionStorage.getItem("locale") == "ar"
                                            ? '/aboutus/?locale=ar'
                                            : '/aboutus'}
                                            onClick={true}>
                                            <p className="navtop-item"><FormattedMessage id='aboutus'/></p>
                                        </Link>
                                    </div>
                                    <div className="p-2">
                                        <button className="langbtn" onClick={this.openLoginModal}><FormattedMessage id='login'/></button>
                                    </div>
                                    <div className="p-2">
                                        <button className="langbtn" onClick={this.openSignupModal}><FormattedMessage id='getstarted'/></button>
                                    </div>
                                    <div
                                        className="p-2"
                                        style={{
                                        marginTop: 20
                                    }}>
                                        {(sessionStorage.getItem("locale") === "ar"
                                            ? (
                                                <div
                                                    className="row"
                                                    style={{
                                                    marginRight: 5
                                                }}>
                                                    <div className="col">
                                                        <img
                                                            onClick={() => {
                                                            this.changeLangueEn()
                                                        }}
                                                            src="../../images/en.png"
                                                            style={{
                                                            height: 25,
                                                            width: 25
                                                        }}/>
                                                    </div>
                                                    <div className="col">
                                                        <h1
                                                            className="langueEN"
                                                            onClick={() => {
                                                            this.changeLangueEn()
                                                        }}>English</h1>
                                                    </div>
                                                </div>
                                            )
                                            : (
                                                <div
                                                    className="row"
                                                    style={{
                                                    marginLeft: 5
                                                }}>
                                                    <div className="col">
                                                        <img
                                                            onClick={() => {
                                                            this.changeLangueAr()
                                                        }}
                                                            src="../../images/ar.png"
                                                            style={{
                                                            height: 25,
                                                            width: 25
                                                        }}/>
                                                    </div>
                                                    <div className="col">
                                                        <h1
                                                            className="langueAR"
                                                            onClick={() => {
                                                            this.changeLangueAr()
                                                        }}>العربية</h1>
                                                    </div>
                                                </div>
                                            ))
}
                                    </div>
                                </div>
}

                        </div>
                    </div>
                </div>

                {/*Mobile menu -----------------------------------------------------------------------------------*/}
                {this.state.menumobile
                    ? (this.state.isloggedin
                        ? (
                            <div
                                style={{
                                backgroundColor: '#fafafa'
                            }}
                                className="mobileitem">
                                <div
                                    className="container"
                                    style={{
                                    padding: 20
                                }}>
                                    <button
                                        type="submit"
                                        className="langbtnmenu"
                                        onClick={this.createjob}
                                        value="Create an article">
                                        <h1><FormattedMessage id='Createanarticle'/></h1>
                                    </button>
                                    <hr
                                        style={{
                                        marginTop: 20
                                    }}/>
                                    <Link to='/'>
                                        <h1 style={elements}>
                                            Home
                                        </h1>
                                    </Link>
                                    <Link to="/explore">
                                        <h1 style={elements}>
                                            Explore
                                        </h1>
                                    </Link>
                                    <hr
                                        style={{
                                        marginTop: 20
                                    }}/>
                                    <Link to='/profile'>
                                        <h1 style={elements}>
                                            Profile
                                        </h1>
                                    </Link>
                                    <hr
                                        style={{
                                        marginTop: 20
                                    }}/>
                                    <Link to='/editPassword'>
                                        <h1 style={elements}>Change password</h1>
                                    </Link>
                                    <hr
                                        style={{
                                        marginTop: 20
                                    }}/>
                                    <form action="">
                                        <button type="submit" style={elements} onClick={this.logout} value="Signout">
                                            <a href="">Sign out</a>
                                        </button>
                                    </form>
                                    <div
                                        style={{
                                        backgroundColor: '#fafafa',
                                        height: 20
                                    }}></div>
                                </div>
                            </div>

                        )
                        : (
                            <div
                                style={{
                                backgroundColor: '#fafafa'
                            }}
                                className="mobileitem">
                                <div
                                    className="container"
                                    style={{
                                    padding: 20
                                }}>
                                    <h1 style={elements} onClick={this.openSignupModal}>
                                        Get started
                                    </h1>
                                    <h1 style={elements} onClick={this.openLoginModal}>
                                        Log in
                                    </h1>
                                    <hr
                                        style={{
                                        marginTop: 20
                                    }}/>
                                    <Link to="/explore">
                                        <h1 style={elements}>
                                            Explore
                                        </h1>
                                    </Link>
                                    <Link to="/aboutus">
                                        <h1 style={elements}>
                                            About us
                                        </h1>
                                    </Link>
                                    <Link to="/contact">
                                        <h1 style={elements}>
                                            Contact
                                        </h1>
                                    </Link>
                                    <hr
                                        style={{
                                        marginTop: 20
                                    }}/>
                                    <div
                                        style={{
                                        backgroundColor: '#fafafa',
                                        height: 20
                                    }}></div>
                                </div>
                            </div>
                        ))
                    : (null)
}

                {/*Drop down menu -----------------------------------------------------------------------------------*/}
                {this.state.showMenu
                    ? (
                        <div
                            className={sessionStorage.getItem("locale") == "ar"
                            ? "myMenuArabic withShadow"
                            : "myMenu withShadow"}
                            ref={(element) => {
                            this.dropdownMenu = element;
                        }}>
                            <div
                                className={sessionStorage.getItem("locale") == "ar"
                                ? "myMenuArrowUpArabic"
                                : "myMenuArrowUp"}></div>
                            <div className="container">
                                <div className="row">
                                    <div className="col-12">
                                        <h1
                                            className="myInputTextMenu1"
                                            style={{
                                            marginTop: 30
                                        }}>Create a job</h1>
                                    </div>
                                    <div className="col-12">
                                        <h2
                                            className="myText"
                                            style={{
                                            marginTop: 10
                                        }}>Duis
                                            esse exercitation cupidatat dolore sunt enim nisi eiusmod velit ut nostrud do
                                            voluptate laboris.</h2>
                                    </div>
                                    <div className="col-12">
                                        <button
                                            type="submit"
                                            className="langbtnmenu"
                                            style={{
                                            marginBottom: 10
                                        }}
                                            onClick={this.createjob}
                                            value="Create an article">
                                            <h1><FormattedMessage id='Createanarticle'/></h1>
                                        </button>
                                    </div>

                                    <div className="col-12 rectanclemenu">
                                        <Link
                                            to={sessionStorage.getItem("locale") == "ar"
                                            ? '/profile/?locale=ar'
                                            : '/profile'}
                                            onClick={true}>
                                            <h4
                                                className="mb12Regular"
                                                style={{
                                                textAlign: sessionStorage.getItem("locale") == "ar"
                                                    ? "right"
                                                    : "left",
                                                marginTop: 10
                                            }}>
                                                <FormattedMessage id='Profile'/></h4>
                                        </Link>
                                    </div>
                                    <div className="col-12 rectanclemenu">
                                        <Link
                                            to={sessionStorage.getItem("locale") == "ar"
                                            ? '/editPassword/?locale=ar'
                                            : '/editPassword'}
                                            onClick={true}>
                                            <h4
                                                className="mb12Regular"
                                                style={{
                                                textAlign: sessionStorage.getItem("locale") == "ar"
                                                    ? "right"
                                                    : "left",
                                                marginTop: 10
                                            }}><FormattedMessage id='Changepassword'/></h4>
                                        </Link>
                                    </div>
                                    <hr/>
                                    <div
                                        className="col-12 rectanclemenu"
                                        style={{
                                        marginBottom: 20,
                                        textAlign: sessionStorage.getItem("locale") == "ar"
                                            ? "right"
                                            : "left"
                                    }}>
                                        <h4
                                            className="mb12Regular"
                                            onClick={this.logout}
                                            style={{
                                            marginTop: 10
                                        }}><FormattedMessage id='singout'/></h4>
                                    </div>
                                </div>

                            </div>
                        </div>
                    )
                    : (null)
}

                {/*Notification menu -----------------------------------------------------------------------------------*/}
                {this.state.showNotificationMenu
                    ? (
                        <div
                            className={sessionStorage.getItem("locale") == "ar"
                            ? "notficationMenuArabic withShadow"
                            : "notficationMenu withShadow"}
                            ref={(element) => {
                            this.dropdownMenu = element;
                        }}>
                            <div
                                className={sessionStorage.getItem("locale") == "ar"
                                ? "myNotificationMenuArrowUpArab"
                                : "myNotificationMenuArrowUp"}></div>
                            <div className="container">

                                {notificationsItems}

                                <hr
                                    style={{
                                    marginTop: 10,
                                    marginBottom: 10
                                }}/>

                                <div
                                    className="row text-center mb12Regular d-flex justify-content-center"
                                    style={{
                                    marginBottom: 20,
                                    fontWeight: "bold"
                                }}>
                                    <Link
                                        to={{
                                        pathname: sessionStorage.getItem("locale") === "ar"
                                            ? "/Notification?locale=ar"
                                            : "/Notification",
                                        state: {
                                            fromArticle: false,
                                            article_id: "",
                                            article_name: ""
                                        }
                                    }}
                                        onClick={true}>
                                        <FormattedMessage id='seemore'/>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    )
                    : (null)
}
            </div>
        )
    }

    componentWillMount() {
        if (localStorage.getItem('account')) {
            this.setState({
                account: JSON.parse(localStorage.getItem('account'))
            })
        }

        if (sessionStorage.getItem('isloggedin') == "true") {
            this.setState({isloggedin: true})
            this._getNotifications()
        } else {
            this.setState({isloggedin: false})
        }

        if (sessionStorage.getItem('isProfessional') == "true") {
            this.setState({isProfessional: true})
        } else {
            this.setState({isProfessional: false})
        }
    }

    componentDidMount() {
        if (sessionStorage.getItem('isloggedin') == "true") {
            this.setState({isloggedin: true})
            this._getNotifications()
        } else {
            this.setState({isloggedin: false})
        }

        this.interval = setInterval(() => {
            if (sessionStorage.getItem('isloggedin') == "true") {
                this.setState({isloggedin: true})
                this._getNotifications()
            } else {
                this.setState({isloggedin: false})
            }
        }, 5000)

    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    login(email, password) {
        let options = {}

        options.method = 'POST'

        options.headers = {
            'Accept': 'application/json',
            'Content-type': 'application/json'
        }

        options.body = JSON.stringify({email, password})

        fetch("http://127.0.0.1:3000/users/authentication", options)
            .then(res => res.json())
            .then(res => {
                if (res.hits.hits.length != 0) {
                    var obj = res.hits.hits;
                    var account = obj[0]._source;

                    if (account.mailvalidation) {

                        if (!account.blocked) {
                            account.id = obj[0]._id;
                            account.token = res.token;
                            this.setState({account: account});
                            localStorage.setItem('account', JSON.stringify(account));
                            localStorage.setItem('nameuser', account.name);

                            this._getNotifications()

                            if (account.modules) {
                                if (account.modules.length != 0) {
                                    this.setState({isProfessional: true})

                                    if (account.firstrun === true) {
                                        console.log("1st run : true");
                                        this.closeModal();
                                        this.openProfessionalUserModal();
                                    } else {
                                        console.log("1st run : false");
                                        this.closeModal();
                                    }

                                } else {
                                    this.setState({isProfessional: false})
                                    this.closeModal();
                                }
                            } else {
                                this.setState({isProfessional: false})
                                this.closeModal();
                            }

                            sessionStorage.setItem('isProfessional', this.state.isProfessional)

                            this.setState({isloggedin: true})
                            sessionStorage.setItem('isloggedin', this.state.isloggedin)

                            location.reload();
                            //this.props.history.push('/');
                        } else {
                            toastr.clear();
                            toastr.info("Your account is blocked by the admin, \
 for more information contact the admin.");
                        }
                    } else {
                        toastr.clear();
                        toastr.info("Your Email is not valide");
                    }
                } else {
                    toastr.clear();
                    toastr.error("Can't find you. Whould you like to signup?");
                }

            })
            .catch(err => {
                console.error(err)
            })
    }

    _openNewJob() {
        this
            .props
            .history
            .push('/newJob');
    }

    openprofile() {
        this
            .props
            .history
            .push('/profile');
    }

    createjob() {
        this
            .props
            .history
            .push('/newJob');
    }

    logout() {
        sessionStorage.setItem('isloggedin', false)
        this.setState({account: ''})
        this
            .props
            .history
            .push(sessionStorage.getItem("locale") == "ar"
                ? '/?locale=ar'
                : '/');
        localStorage.removeItem('account');
        location.reload();
    }

    _getNotifications() {
        
    }

    _readNotification(notification_id) {
        
                        this
                            .props
                            .history
                            .push({
                                pathname: "/Notification",
                                state: {
                                    fromArticle: false,
                                    article_id: "",
                                    article_name: ""
                                }
                            });
    }

    openmenu() {
        this.setState({menumobile: true})
    }

    closemenu() {
        this.setState({menumobile: false})
    }

    openProReqModal() {
        this.setState({showMenu: false, signupPageIsOpen: false, loginPageIsOpen: false, professionlPageIsOpen: false, proReqPageIsOpen: true});
    }

    openSignupModal() {
        this.setState({
            signupPageIsOpen: true,
            loginPageIsOpen: false,
            proReqPageIsOpen: false,
            professionlPageIsOpen: false,
            testclickbtn: false,
            testclickbtngetstarted: true
        });
    }

    openProfessionalUserModal() {
        this.setState({professionlPageIsOpen: true, signupPageIsOpen: false, loginPageIsOpen: false, proReqPageIsOpen: false});
    }

    openLoginModal() {
        this.setState({
            loginPageIsOpen: true,
            signupPageIsOpen: false,
            proReqPageIsOpen: false,
            professionlPageIsOpen: false,
            testclickbtn: true,
            testclickbtngetstarted: false
        });
    }

    closeModal() {
        this.setState({loginPageIsOpen: false, signupPageIsOpen: false, proReqPageIsOpen: false, professionlPageIsOpen: false});
    }

    showMenu(event) {
        event.preventDefault();

        this.setState({
            showMenu: true,
            showNotificationMenu: false
        }, () => {
            document.addEventListener('click', this.closeMenu);
        });
    }

    showNotificationMenu(event) {
        event.preventDefault();

        this.setState({
            showMenu: false,
            showNotificationMenu: true
        }, () => {
            document.addEventListener('click', this.closeMenu);

        });
    }

    closeMenu() {
        this.setState({
            showMenu: false,
            showNotificationMenu: false
        }, () => {
            document.removeEventListener('click', this.closeMenu);
        });
    }

    goHome() {
        this
            .props
            .history
            .push("/");
    }

    changeLangueAr() {

        this
            .props
            .history
            .push("?locale=ar");
        location.reload();
    }

    changeLangueEn() {
        this
            .props
            .history
            .push("?locale=en");
        location.reload();
    }
}
export default withRouter(Navtop)
injectIntl(Navtop);