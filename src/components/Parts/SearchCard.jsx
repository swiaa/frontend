import React from 'react'


import {BookmarkIconHeader} from '../../images/BookmarkIconHeader.svg'
import {BookmarkedIcon} from '../../images/BookmarkedIcon.svg'
import {BrowserRouter as Router, Route, Link, withRouter} from "react-router-dom";

import FontAwesome from 'react-fontawesome'

const mainTitle = {

    fontFamily: "mbbold",
    fontSize: 19.2,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 2.8,
    letterSpacing: 0.21,
    textAlign: "left",
    color: "#3c3d41"
};
const paragraph = {

    fontFamily: "mbregular",
    fontSize: 10.5,
    fontWeight: "300",
    fontStyle: "normal",
    letterSpacing: 0.11,
    textAlign: "justify",
    color: "#3c3d41",
    marginBottom: 18
};

const byAuthorName = {
    fontFamily: "mbregular",
    fontSize: 11,
    fontWeight: "normal",
    fontStyle: "normal",
    letterSpacing: 0.07,
    textAlign: "left",
    color: "#838d8f"
};
class SearchCard extends React.Component {

    constructor(props) {
        super(props)

        this.state = {}
        this._navigateToArticle = this
            ._navigateToArticle
            .bind(this);
    }

    _navigateToArticle(id) {
        this
            .props
            .history
            .push('/article/' + id);
    }

    render() {

        return (
            <div >
                <div
                    style={{
                    width: 640,
                    height: 180,
                    marginTop: 30
                }}
                    className="offresponsive">
                    <div className="row">
                        <div
                            className="col-md-3"
                            onClick={() => {
                            this._navigateToArticle(this.props.data._id)
                        }}>
                            <img
                                src={this.props.data._source.thumbnail}
                                style={{
                                objectFit: "cover",
                                height: 180,
                                width: 180
                            }}/>
                        </div>
                        <div className="col">
                            <div
                                class="container"
                                style={{
                                backgroundColor: 'white',
                                height: 180,
                                paddingLeft: 28
                            }}>
                                <div className="row">
                                    <div
                                        className="categoryslider2"
                                        align="center"
                                        style={{
                                        width: 72
                                    }}>
                                        <br></br>
                                        <p className="txtcategory">{this.props.data._source.typecat}
                                        </p>
                                    </div>
                                </div>
                                <h1 style={mainTitle}>{this.props.data._source.title}</h1>
                                <p style={paragraph}>{this.props.data._source.description}</p>
                                <hr
                                    style={{
                                    marginBottom: 12
                                }}/>
                                <div >
                                    {(!this.props.data._source.nameuser || this.props.data._source.nameuser == "" || this.props.data._source.nameuser == "undifined")
                                        ? (
                                            <p style={byAuthorName} class="byauthor">
                                                By {this.props.data._source.nameExpertConfirmedBy}</p>
                                        )
                                        : (
                                            <p style={byAuthorName} class="byauthor">
                                                By {this.props.data._source.nameuser}</p>
                                        )
}
                                    <FontAwesome className='bookmarkFA' name='bookmark' size='1x'/>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div
                    style={{
                    width: '100%',
                    height: 305,
                    marginTop: 30,
                    marginBottom: 30,
                    marginRight: 20
                }}
                    className="mobileitem">

                    <div
                        onClick={() => {
                        this._navigateToArticle(this.props.data._id)
                    }}
                        style={{
                        backgroundImage: 'url(' + this.props.data._source.thumbnail + ')'
                    }}
                        class="backgroundcard withBGCover">
                        <div
                            className="categoryslider"
                            align="center"
                            style={{
                            width: 72
                        }}>
                            <br></br>
                            <p className="txtcategory">{this.props.data._source.typecat}
                            </p>
                        </div>
                    </div>

                    <div
                        class="container cardcontainer"
                        style={{
                        backgroundColor: 'white',
                        height: 145
                    }}>
                        <h1 style={mainTitle}>{this.props.data._source.title}</h1>

                        <p style={paragraph}>{this.props.data._source.description}</p>
                        <hr
                            style={{
                            marginBottom: 12
                        }}/>
                        <div >
                            {(!this.props.data._source.nameuser || this.props.data._source.nameuser == "" || this.props.data._source.nameuser == "undifined")
                                ? (
                                    <p style={byAuthorName} class="byauthor">
                                        By {this.props.data._source.nameExpertConfirmedBy}</p>
                                )
                                : (
                                    <p style={byAuthorName} class="byauthor">
                                        By {this.props.data._source.nameuser}</p>
                                )
}

                            <FontAwesome className='bookmarkFA' name='bookmark' size='1x'/>
                        </div>
                    </div>
                </div>
            </div>
        )

    }

}

export default withRouter(SearchCard)