import React from 'react'
import {BrowserRouter as Router, Route, Link, withRouter} from "react-router-dom";

import {FormattedMessage} from 'react-intl';

const textStyle = {
    fontFamily: "JF Flat Jozoor",
    fontSize: 20,
    fontStyle: "normal",
    letterSpacing: 0.41,
    color: '#F0E40F',
    textAlign: sessionStorage.getItem("locale") === "ar"
        ? ('left')
        : ('right')

};

class Explorecategory extends React.Component {

    constructor(props) {
        super(props)
        this.state = {}

    }

    render() {

        return (
            <div
                className="container-fluid stylecontainer"
                style={{
                paddingTop: 40,
                paddingBottom: 40
            }}>
                <div className="container">
                    <div className="row">
                        <div className="col-4 d-flex justify-content-start">
                            <span style={textStyle}>1234 Offer</span>
                        </div>
                        <div className="col-4 offset-md-4 d-flex justify-content-end">
                            <span style={textStyle}>1234 User</span>
                        </div>
                    </div>
                </div>
            </div>
        )

    }

}

export default withRouter(Explorecategory)