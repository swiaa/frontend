/**
 * Author : Aymen FEZAI
 */

import React, {Component} from 'react';
import {FormattedMessage} from 'react-intl';
class TeamCard extends Component {
    render() {
        return (
            <div className="col-md-3 col-6">
                <div
                    className="card"
                    style={{
                    padding: 20,
                    marginTop: 20,
                    border: "none",
                    direction: sessionStorage.getItem("locale") === "ar"
                        ? "rtl"
                        : "ltr"
                }}>
                    <div className="d-flex justify-content-center">
                        <img
                            className="card-img-top rounded-circle img-fluid"
                            style={{
                            width: 150,
                            height: 150
                        }}
                            src="../../images/CategoryCardPhysiognomy.png"
                            alt=""/>
                    </div>
                    <div className="d-flex justify-content-center">
                        <h5
                            class="card-title mySmallTitle"
                            style={{
                            marginTop: 20
                        }}><FormattedMessage id='eli'/></h5>
                    </div>
                    <div className="d-flex justify-content-center">
                        <h6
                            class="card-title mb14Regular text-center"
                            style={{
                            marginTop: 0
                        }}><FormattedMessage id='eli'/></h6>
                    </div>
                </div>
            </div>
        );
    }
}

export default TeamCard;