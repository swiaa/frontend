import React from 'react'
import Autosuggest from 'react-autosuggest'
import "../../images/headerImage.jpg";
import '../../images/SearchIcon.png';
import {Parallax, Background} from 'react-parallax';

import {BrowserRouter as Router, Route, Link, withRouter} from "react-router-dom";
import {FormattedMessage} from 'react-intl';

class header extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            suggestions: [],
            value: ''
        }

    }

    render() {

        const {value, suggestions} = this.state

        const autoFocus = 'autofocus'

        const inputProps = {

            placeholder: sessionStorage.getItem("locale") === "ar"
                ? "بحث الثقافة والفنون ..."
                : "Search articles, images and more ...",
            value,
            onChange: this.onChange,
            autoFocus,
            height: 200
        }

        return (

            <div>
                <div className="offresponsive">

                    <Parallax bgImage='../images/headerImage.jpg' bgStyle={{backgroundPosition: "center"}} strength={200}>
                        <form action="">

                            <div
                                className="row col-md-12 align-items-center"
                                style={{
                                height: this.props.dimension.d1,
                                direction: sessionStorage.getItem("locale") === "ar"
                                    ? "rtl"
                                    : "ltr"
                            }}>
                                <div className="col-md-3"></div>

                                <div className="col-md-6">
                                    <div className="row" align="center">
                                        <div className="col">
                                            <h1 className="h1Title" style={{width:400, border:"2px solid #F0E40F", padding:10, cursor: "pointer"}}><a href="/aboutus">How Swiaa works?</a></h1>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3"></div>
                            </div>
                        </form>
                    </Parallax>
                </div>

            </div>

        )

    }

}

export default withRouter(header)
