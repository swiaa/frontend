//Everything will go to hell from now on

import React from 'react'

import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import {loadimage} from '../../images/loading_state.png'
import {loadimageGreen} from '../../images/loadinggreen.svg'
import TodayCollectionCard from './TodayCollectionCard.jsx'

import Autosuggest from 'react-autosuggest'
import SearchCard from './SearchCard.jsx';
// import Pagination from './Pagination.jsx';
import Test from './../Parts/testfooter.jsx'

import {BrowserRouter as Router, Route, Link, withRouter} from "react-router-dom";
import {Parallax, Background} from 'react-parallax';
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';
import 'react-tabs/style/react-tabs.css';

import Masonry from 'react-masonry-component';
import Modal from 'react-modal';
import {FormattedMessage} from 'react-intl';
import {Pagination, PaginationItem, PaginationLink} from 'reactstrap';

import Loader from 'react-loader'

const resultsFound = {
    fontFamily: "JF Flat Jozoor",
    fontSize: 24,
    marginTop: 20,
    marginBottom: 30,
    fontStyle: "normal",
    letterSpacing: 0.41,
    color: '#3c3d41',
    textAlign: sessionStorage.getItem("locale") === "ar"
        ? ("right")
        : ("left")
};

const masonryOptions = {
    transitionDuration: 10
};

const mainTitle = {
    fontFamily: "JF Flat Jozoor",
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 2.8,
    letterSpacing: 0.21,
    textAlign: "left",
    color: "#3c3d41",
    width: "100%",
    textOverflow: "ellipsis",
    overflow: "hidden",
    whiteSpace: "nowrap"
};

const paragraph = {
    fontFamily: "mbregular",
    fontSize: 12,
    fontWeight: "300",
    fontStyle: "normal",
    letterSpacing: 0.15,
    textAlign: "justify",
    color: "#3c3d41",
    marginBottom: 18,
    width: "100%",
    lineHeight: "normal"
};

const byAuthorName = {
    fontFamily: "mbregular",
    fontSize: 11,
    fontWeight: "normal",
    fontStyle: "normal",
    letterSpacing: 0.07,
    textAlign: "left",
    color: "#838d8f",
    width: "100%",
    textOverflow: "ellipsis",
    overflow: "hidden",
    whiteSpace: "nowrap"
};

const customStyles = {
    content: {
        zIndex: 5000
    },
    overlay: {
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        zIndex: 5000
    }
};

class SearchScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            keyword: "",
            value: "",
            total: 0,
            tabIndex: 0,
            currentPageImage: 1,
            pagesImage: null,
        };
    }

    _searchImage(pageIndex) {
            let pagesImage = 14; //Pagination Image
            this.setState({pagesImage: pagesImage, currentPageImage: pageIndex}); //Pagination Image

            for(let i=0; i<pagesImage;i++) {
                return (
                    <li key={i}>
                        <div
                            className="card withShadow"
                            style={{
                            width: 350,
                            margin: 20
                        }}
                            onClick={() => this.openModalimg(i)}>
                            <img
                                className="card-img-top"
                                src="../../images/demenagement.jpg"
                                style={{
                                objectFit: "cover",
                                height: 300,
                                width: "100%"
                            }}
                                alt="Title"/>
                            <div className="card-body">
                                <h1 style={mainTitle}>Title</h1>
                                <hr
                                    style={{
                                    margin: "10px 0px"
                                }}/>
                                <p style={paragraph}>
                                    Nostrud ex cillum fugiat laborum culpa aliqua ullamco ea. Duis adipisicing ut qui id ullamco fugiat sit ea reprehenderit duis nulla. Cillum cillum minim fugiat id eiusmod sit excepteur nulla do ex minim amet et.
                                </p>
                                <hr
                                    style={{
                                    margin: "10px 0px"
                                }}/>
                                <p style={byAuthorName}>
                                    <span
                                        style={{
                                        fontWeight: "bold"
                                    }}>Author :
                                    </span>
                                    Foulen
                                </p>
                            </div>
                        </div>
                    </li>
                )
            }

            this.setState({total: 14});
    }

    onChange = (event, {newValue, method}) => {
        this.setState({value: newValue})
    }

    /**
     * Pagination Image
     */

    handlePageClickImage(pageIndex) {
        window.scroll({top: 0, left: 0, behavior: 'smooth'});
        this.setState({currentPageImage: pageIndex, loaded: false});
        this._searchImage(pageIndex);
    }

    render() {
        let pagination2 = [];

        for (let pageIndex = 1; pageIndex <= this.state.pagesImage; pageIndex++) {
            pagination2.push(
                <PaginationItem
                    key={pageIndex}
                    active={this.state.currentPageImage == pageIndex
                    ? true
                    : false}>
                    <PaginationLink onClick={() => this.handlePageClickImage(pageIndex)}>
                        {pageIndex}
                    </PaginationLink>
                </PaginationItem>
            );
        }

        const {value, suggestionsBeta} = this.state
        const autoFocus = 'autofocus'
        const inputProps = {
            placeholder: "Search for a job ...",
            value,
            onChange: this.onChange,
            autoFocus,
            height: 200
        }

        return (

            <div>

                <Parallax bgImage="../../images/headerImage.jpg" strength={200}>
                    <div
                        style={{
                        paddingTop: 60,
                        paddingBottom: 60
                    }}>
                        <center>

                            <span className="Categorytitle">
                                <h1
                                    style={{
                                    textTransform: 'uppercase'
                                }}>{this.state.catsearch}</h1>
                            </span>
                        </center>
                        <form action="">
                            <div
                                className="row col-md-12 align-items-center"
                                style={{
                                height: 180
                            }}>

                                <div className="col-md-3"></div>
                                <div
                                    className="row col-md-6"
                                    style=
                                    {{ height: 62, borderRadius: 6, backgroundColor: "rgba(255, 255, 255, 0.2)", paddingTop:6, direction: sessionStorage.getItem("locale")==="ar"? "rtl":"ltr" }}>
                                    <div className="col">
                                        <div className='grid-input'>
                                            <Autosuggest
                                                suggestions={suggestionsBeta}
                                                onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                                                onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                                                onSuggestionSelected={this.onSuggestionSelected}
                                                getSuggestionValue={this.getSuggestionValue}
                                                renderSuggestion={this.renderSuggestion}
                                                inputProps={inputProps}/>
                                        </div>
                                    </div>
                                    <div
                                        className="col-2"
                                        align="right"
                                        style={{
                                        padding: 0
                                    }}
                                        onClick={this._search}>
                                        <button
                                            type="submit"
                                            className="searchbtn"
                                            style={{
                                            marginTop: 0,
                                            width: "100%"
                                        }}><FormattedMessage id='search'/></button>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </Parallax>

                <div className="container-fluid">
                    <div
                        className="container-fluid"
                        style={{
                        marginTop: 30,
                        marginBottom: 60
                    }}>
                        {this.state.loaded
                            ? <div>
                                    <div className="row">
                                        <h1 style={resultsFound}>{this.state.total}<FormattedMessage id='Resultsfound'/></h1>
                                    </div>

                                    <Masonry options={masonryOptions} elementType={'ul'} // default 'div'
                                        disableImagesLoaded={false} // default false
                                        updateOnEachImageLoad={false} // default false and works only if disableImagesLoaded is false
                                    >
                                        {this.state.suggestions}
                                    </Masonry>
                                </div>
                            : <img src={this.state.emptyStateImage} width="100%"/>
}
                    </div>

                    <div
                        className="row d-flex justify-content-center"
                        style={{
                        paddingBottom: 50
                    }}>
                        <Pagination>
                            <PaginationItem
                                disabled={this.state.currentPageImage == 1
                                ? true
                                : false}>
                                <PaginationLink
                                    previous
                                    onClick={() => this.handlePageClickImage(this.state.currentPageImage - 1)}/>
                            </PaginationItem>
                            {pagination2}
                            <PaginationItem
                                disabled={this.state.currentPageImage == this.state.pagesImage
                                ? true
                                : false}>
                                <PaginationLink
                                    onClick={() => this.handlePageClickImage(this.state.currentPageImage + 1)}
                                    next/>
                            </PaginationItem>
                        </Pagination>
                    </div>
                </div>

                <Test/>
            </div>

        )
    }
}

export default withRouter(SearchScreen)
