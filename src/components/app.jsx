import React from 'react';
import Navtop from './Parts/Navtop.jsx';
import {withRouter} from 'react-router-dom';
import Main from './Parts/Main.jsx';

class App extends React.Component {
    render() {
        return (
            <div>
                <Navtop/>
                <Main/>
            </div>
        )
    }
}
export default withRouter(props => <App {...props}/>);
