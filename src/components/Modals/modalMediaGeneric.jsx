import React from 'react'

import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import {leftarrow} from '../../images/arrow_left.png'
import {rightarrow} from '../../images/arrow_right.png'
import {playaudio} from '../../images/PlayMedia_iPad.png'
 
import { Document ,Page} from 'react-pdf/dist/entry.webpack';

import Modal from 'react-modal';
import Loader from 'react-loader';

const customStyles = {
    content : {
        zIndex: 5000,
    },
    overlay: {
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        zIndex: 5000
    },
};
  
export default class Modalcustom extends React.Component { 

    constructor(props) {
        super(props)

        this.state = {
            modalIsOpen: this.props.modalview.modalMediaIsOpen , 
            showmusicplayer:true,
            element : 0 ,
            numPages: null,
            pageNumber: 1,
            urlpdf: '',
            loaded:true,
        };

        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.nextelement = this.nextelement.bind(this);
        this.prevelement = this.prevelement.bind(this);
        this.startaudio = this.startaudio.bind(this);

    }

    onDocumentLoad = ({ numPages }) => {
        this.setState({ numPages });
    }

    
    startaudio ()
    {   
        this.setState({loaded : false});
        this.setState({
            showmusicplayer:false
        })
        var wavesurfer = WaveSurfer.create({
            container: '#waveform'
        });
        wavesurfer.load('https://storage.googleapis.com/culturestorage/advertising.mp3');
        setTimeout(() => {
            wavesurfer.playPause();
        }, 1000);
       
    }

    
    openModal() {
    this.setState({modalIsOpen: true});
    }
     
    afterOpenModal() { 
    // this.subtitle.style.color = '#f00';
    }
     
    closeModal() {
    this.props.closeModalNow(false);
    }


    nextelement () 
    {
        this.setState({
            showmusicplayer:true
        })
        if (this.props.data.length-1 > this.state.element )      
                this.setState ({ element: this.state.element + 1 }) ;
        
                
        

    }

    prevelement () 
    {
    this.setState({
        showmusicplayer:true
    })
        if ( this.state.element > 0 )
        {


        this.setState ({
            element: this.state.element - 1
        })
    

    }
    
    }


      render() {

        const { pageNumber, numPages } = this.state;
        
        return ( 
          
            <Modal
                isOpen={this.props.modalview.modalMediaIsOpen}
                onRequestClose={this.closeModal}
                className="custommodal"
                style={customStyles}>
                  
                <h2 ref={subtitle => this.subtitle = subtitle}></h2>
                <Loader loaded={this.state.loaded}>
         
                <div className="container row" style={{padding:0,margin:0,backgroundColor:'#0f1c14'}}>
                    <div className="col-md-8 col-xs-12 col-sm-12" style={{padding:0}}>
                    {
                        (this.props.data[this.state.element]._source.mediatype == 'Video') ?
                            <div>
                                <div  onClick={this.nextelement}>
                                    <img src="../images/arrow_right.png"  className="navbttnsr" />
                                </div>
                                <div  onClick={this.prevelement}>
                                    <img src="../images/arrow_left.png"  className="navbttnsl" />
                                </div>
                                <video  style={{width:'100%', height:500}} controls src={this.props.data[this.state.element]._source.url}>
                                </video>
                               
                            </div>
                    
                        :

                        <div>
                            { 
                                ((this.props.data[this.state.element]._source.mediatype == 'audio')||(this.props.data[this.state.element]._source.mediatype == 'Sound'))  ? 
                                    <div>
                                        <div  onClick={this.nextelement}>
                                            <img src="../images/arrow_right.png"  className="navbttnsr" />
                                            </div>
                                            <div  onClick={this.prevelement}>
                                            <img src="../images/arrow_left.png"  className="navbttnsl" />
                                            </div>

                                        { 
                                            (this.state.showmusicplayer)?

                                            <div  className="containerplaysong">
                                                <audio
                                                    style={{width:'100%', height:100}}
                                                    id="t-rex-roar"
                                                    controls
                                                    src={this.props.data[this.state.element]._source.url}>
                                                    Your browser does not support the <code>audio</code> element.
                                                </audio>
                                               
                                            </div>
                                        :
                                            <div>
                                               
                                            </div>
                                        
                                        }
                                    
                                        <div id="waveform" style={{backgroundColor:'white',marginTop:'25%'}}> </div>
                                    </div>

                                :

                                    <div>
                                        { 
                                            (this.props.data[this.state.element]._source.mediatype == 'Image')?
                                                <div>
                                                    <div  onClick={this.nextelement}>
                                                    <img src="../images/arrow_right.png"  className="navbttnsr" />
                                                    </div>
                                                    <div  onClick={this.prevelement}>
                                                    <img src="../images/arrow_left.png"  className="navbttnsl" />
                                                    </div>
                                                    <img src={this.props.data[this.state.element]._source.url} style={{width:120,height:500}}/>  
                                                  
                                                </div>
                                            :
                                            <div>
                                               </div>
                                        }
                                    </div>
                                    
 
                            }
                            {
                              (this.props.data[this.state.element]._source.mediatype =='Document')?
                              <div>
                             <iframe src={"http://docs.google.com/gview?url="+this.props.data[this.state.element]._source.url+"&embedded=true"} frameborder="1" scrolling="auto" style={{width:"100%",height:500}}></iframe>
                               </div> 
                              :
                            (null)
                            }

                        </div>
                        
                        
                         
                    }
                    </div> 


                    <div style={{backgroundColor:'#0f1c14', width:819, padding:0, overflow:"auto", height:500, overflowX: "hidden"}} className="col-md-4 col-xs-12 col-sm-12">
                        <div className="row">
                        <div>
                           
                        </div>
                            <div className="col"  style={{marginTop:30, marginBottom:30}}>                               
                                <p className="subtitle2" style={{marginLeft:44,color:'white'}}>{this.props.data[this.state.element]._source.name}</p>                                

                                <div style={{height:10}}></div>
                                <h2 className="subtitle" style={{marginLeft:44,marginBottom:10}}>Media source</h2>
                                <p className="subtitle2" style={{marginLeft:44,color:'white', wordBreak: "break-all"}}>{this.props.data[this.state.element]._source.mediasource}</p>
                                                
                                <div style={{height:10}}></div>
                                <h2 className="subtitle" style={{marginLeft:44,marginBottom:10}}>Author</h2>
                                <p className="subtitle2" style={{marginLeft:44,color:'white'}}>{this.props.data[this.state.element]._source.author}</p>
                                        
                                <h2 className="subtitle" style={{marginLeft:44,marginBottom:10}}>Licenses</h2>
                                <p className="subtitle2" style={{marginLeft:44,color:'white'}}>{this.props.data[this.state.element]._source.licenses}</p>
                                <div style={{height:10}}></div>

                                <div style={{height:10}}></div>
                                <h2 className="subtitle" style={{marginBottom:10,marginLeft:44}}>Description</h2>                                    
                                <div style={{height:45,marginBottom:60}}> 
                                    <p className="subtitle2" style={{marginBottom:10,marginLeft:44,color:'white', textAlign: "justify"}}>{this.props.data[this.state.element]._source.description}</p>    
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                </Loader>
            </Modal>
       
        )

      }

}