/**
 * Auther : Aymen FEZAI
 */

import React, {Component} from 'react';
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import '../../sass/layout/_profilecss.scss'
import Modal from 'react-modal';
import FontAwesome from 'react-fontawesome'
import {FormattedMessage} from 'react-intl';
import '../../images/eye_open.png';
import '../../images/eye_close.png';
//For popup messages
import toastr from 'toastr'
import 'toastr/build/toastr.min.css'

toastr.options = {
    positionClass: 'toast-top-right',
    "closeButton": true,
    hideDuration: 300,
    timeOut: 3000
}
//End of toastr Modal Style
const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        width: '50%',
        maxHeight: "100vh",
        opacity: 1,
        zIndex: 999,
        fontFamily: "JF Flat Jozoor"
    },
    overlay: {
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        zIndex: 999
    }
};

export default class LoginPage extends Component {
    constructor(props) {
        super(props)

        this.state = {
            forgetPwd: false,
            checkPin: false,
            changePwd: false,
            email: '',
            password: '',
            forgetPwd_email: '',
            forgetPwd_pin: '',
            forgetPwd_newPwd: '',
            forgetPwd_confPwd: '',
            pin: '',
            testpwd: '',
            testname: '',
            type: 'password'
        };

        this.login = this
            .login
            .bind(this);
        this.handleChange = this
            .handleChange
            .bind(this);
        this.openforgetPwd = this
            .openforgetPwd
            .bind(this);
        this._sendEmail = this
            ._sendEmail
            .bind(this);
        this._checkpin = this
            ._checkpin
            .bind(this);
        this._changepwd = this
            ._changepwd
            .bind(this);

        this.showHide = this
            .showHide
            .bind(this);
    }

    componentWillMount() {
        this.setState({forgetPwd: false, checkPin: false})
    }

    /***************show pasword  ***********************************/
    showHide(e) {
        e.preventDefault();
        e.stopPropagation();
        this.setState({
            type: this.state.type === 'input'
                ? 'password'
                : 'input'
        })
    }

    /************************************************************************ */
    login(event) {

        event.preventDefault();

        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        /*let email = this.refs.email.value;
        let password = this.refs.password.value;*/

        let email = this.state.email;
        let password = this.state.password;

        /***********************test de verification****************************  */
        if (email == "") {
            document
                .getElementById('email')
                .style
                .borderColor = "red";
            this.setState({testemail: "Email field shouldn't be empty"});
        } else if (!email.match(mailformat)) {
            document
                .getElementById('email')
                .style
                .borderColor = "red";
            this.setState({testemail: "This doesn’t look like an email address.."});
        } else {
            document
                .getElementById('email')
                .style
                .borderColor = "green";
            this.setState({testemail: ""});
        }

        if (password == "") {
            document
                .getElementById('password')
                .style
                .borderColor = "red";
            this.setState({testpwd: "Password field shouldn't be empty"});
        } else {
            document
                .getElementById('password')
                .style
                .borderColor = "green";
            this.setState({testpwd: ""});
        }

        (email == "")
            ? (
            //toastr.info("Email field shouldn't be empty"),
            this.refs.email.classList.add('invalidInput'))
            : ((!email.match(mailformat))
                ? (
                // toastr.info("This doesn’t look  like an email address"),
                this.refs.email.classList.add('invalidInput'))
                : ((password == "")
                    ? (
                    //  toastr.info("Password field shouldn't be empty"),
                    this.refs.password.classList.add('invalidInput'))
                    : (this.props.loginFun(email, password))))
    }

    handleChange(e) {
        e
            .target
            .classList
            .remove('invalidInput')
    }

    /*********************** get value from input ****************************/
    handleChange = name => event => {
        this.setState({[name]: event.target.value});
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        switch (name) {
            case "email":
                if (event.target.value == "") {
                    document
                        .getElementById('email')
                        .style
                        .borderColor = "red";
                    this.setState({testemail: "Email field shouldn't be empty"});
                } else if (!(event.target.value).match(mailformat)) {
                    document
                        .getElementById('email')
                        .style
                        .borderColor = "red";
                    this.setState({testemail: "This doesn’t look like an email address.."});
                } else {
                    document
                        .getElementById('email')
                        .style
                        .borderColor = "green";
                    this.setState({testemail: ""});
                }
                break;
            case "password":
                if (event.target.value == "") {
                    document
                        .getElementById('password')
                        .style
                        .borderColor = "red";
                    this.setState({testpwd: "Password field shouldn't be empty"});
                } else {
                    document
                        .getElementById('password')
                        .style
                        .borderColor = "green";
                    this.setState({testpwd: ""});
                }
                break;
        }
    }

    openforgetPwd() {
        this.setState({forgetPwd: true})
    }

    _sendEmail(event) {
        event.preventDefault()

        let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        //let email = this.refs.forgetPwd_email.value;

        let email = this.state.forgetPwd_email;

        if (!email.match(mailformat)) {
            toastr.info("Email is not valid"),
            this
                .refs
                .forgetPwd_email
                .classList
                .add('invalidInput')
        } else {
            let options = {}
            options.method = 'POST'
            options.headers = {
                'Accept': 'application/json',
                'Content-type': 'application/json',
                'x-access-token': 'gd654dg@fuyffd5'
            }
            options.body = JSON.stringify({email})

            fetch("http://35.204.178.205:4123/users/emailExistence", options).then(res => {
                if (res.ok) {
                    res
                        .json()
                        .then(res => {
                            if (res.message == "exist") {
                                let options = {}
                                options.method = 'GET'
                                options.headers = {
                                    'Accept': 'application/json',
                                    'Content-type': 'application/json',
                                    'x-access-token': 'gd654dg@fuyffd5'
                                }

                                this.setState({email})

                                fetch("http://35.204.178.205:4123/forgetpassword/query/" + email, options).then(res => {
                                    if (res.ok) {
                                        res
                                            .json()
                                            .then(res => {
                                                if (res.status && res.status === 200) {
                                                    this.setState({pin: res.PIN, checkPin: true})
                                                    this.refs.forgetPwd_email.value = '';
                                                } else {
                                                    console.error("Something went wrong!")
                                                }
                                            })
                                    } else {
                                        console.error('Request failed with response ' + res.status + ' : ' + res.statusText);
                                    }
                                }).catch(err => console.error(err))

                            } else {
                                toastr.info("Email doesn't exist")
                                this
                                    .refs
                                    .forgetPwd_email
                                    .classList
                                    .add('invalidInput')
                            }
                        })
                        .catch(err => console.log(err))
                } else {
                    console.error('Request failed with response ' + res.status + ' : ' + res.statusText);
                }
            }).catch(err => console.log(err))
        }
    }

    _checkpin(event) {
        event.preventDefault()

        // let userpin = this.refs.forgetPwd_pin.value;
        let userpin = this.state.forgetPwd_pin;
        let pin = this.state.pin;

        if (userpin.length != 6) {
            this
                .refs
                .forgetPwd_pin
                .classList
                .add('invalidInput')
            toastr.info("Pin must be 6 number")
        } else {
            if (userpin != pin) {
                this
                    .refs
                    .forgetPwd_pin
                    .classList
                    .add('invalidInput')
                toastr.info("Pin is incorrect")
            } else {
                this.setState({changePwd: true})
                this.refs.forgetPwd_pin.value = ''
            }
        }
    }

    _changepwd(event) {
        event.preventDefault();

        /*let newPwd = this.refs.forgetPwd_newPwd.value;
        let confpwd = this.refs.forgetPwd_confPwd.value;*/

        let newPwd = this.state.forgetPwd_newPwd;
        let confpwd = this.state.forgetPwd_confPwd;

        if (newPwd == "" || newPwd != confpwd) {
            this
                .refs
                .forgetPwd_newPwd
                .classList
                .add('invalidInput')
            this
                .refs
                .forgetPwd_confPwd
                .classList
                .add('invalidInput')
            toastr.info("Password doesn't much")
        } else {
            let options = {};
            options.headers = {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': 'gd654dg@fuyffd5'
            };
            options.body = JSON.stringify({email: this.state.email, pin: this.state.pin, password: newPwd});
            options.method = "POST";

            fetch(`http://35.204.178.205:4123/forgetpassword/updatepwd`, options).then(res => {
                if (res.ok) {
                    toastr.success("Done!")
                    this.setState({forgetPwd: false, checkPin: false, changePwd: false})

                    this.refs.forgetPwd_confPwd.value = '';
                    this.refs.forgetPwd_newPwd.value = '';
                } else {
                    console.error('Request failed with response ' + res.status + ' : ' + res.statusText);
                }
            }).catch(err => console.error(err))
        }
    }

    render() {
        var data = this.props.data;
        let form = "";

        if (!this.state.forgetPwd) { //Login Form
            form = <div key="1">
                <div className="row">
                    <div
                        className="col-md-12 col-12 d-flex justify-content-end align-items-start"
                        style={{
                        paddingLeft: 0,
                        paddingRight: 0
                    }}>
                        <button onClick={this.props.closeModal} className="btnCloseSmall">X</button>
                    </div>
                </div>
                <div
                    className="row"
                    style={{
                    marginBottom: 30,
                    marginTop: 20
                }}>

                    <div className="col-md-12 col-12 d-flex justify-content-center">
                        <h1 className="myTitle"><FormattedMessage id='LogintoSwiaa'/></h1>
                    </div>

                </div>

                <form action="">
                    <div
                        className="row"
                        style={{
                        marginBottom: 30
                    }}>
                        <div className="col-md-12 col-12">
                            <FormattedMessage id='email'>
                                {placeholder => (<input
                                    type="text"
                                    id="email"
                                    className="col-md-12 col-12 inputSelect myInputText"
                                    style={{
                                    color: 'black',
                                    height: 60,
                                    textAlign: sessionStorage.getItem("locale") === "ar"
                                        ? ('right')
                                        : ('left')
                                }}
                                    placeholder={placeholder}
                                    value={this.state.email}
                                    onChange={this.handleChange('email')}
                                    ref="email"
                                    autoFocus="true"/>)}
                            </FormattedMessage>
                            <div className="col-md-12 col-12">
                                <h1
                                    style={{
                                    color: "red",
                                    fontSize: 13,
                                    marginTop: 5
                                }}>{this.state.testemail}</h1>
                            </div>
                        </div>
                    </div>

                    <div
                        className="row"
                        style={{
                        marginBottom: 20
                    }}>
                        <div className="col-md-12 col-12">
                            <FormattedMessage id='password'>
                                {placeholder => (<input
                                    id="password"
                                    className=" col-md-12 col-12 inputSelect myInputText"
                                    style={{
                                    color: 'black',
                                    height: 50,
                                    textAlign: sessionStorage.getItem("locale") === "ar"
                                        ? ('right')
                                        : ('left')
                                }}
                                    placeholder={placeholder}
                                    value={this.state.password}
                                    onChange={this.handleChange('password')}
                                    ref="password"
                                    type={this.state.type}/>)}
                            </FormattedMessage>
                            <span
                                className="password__show"
                                onClick={this.showHide}
                                style={{
                                marginTop: 15
                            }}>{this.state.type === 'input'
                                    ? <img
                                            src="../images/eye_close.png"
                                            style={{
                                            width: 20
                                        }}/>
                                    : <img
                                        src="../images/eye_open.png"
                                        style={{
                                        width: 20
                                    }}/>}</span>
                            <div className="col-md-12 col-12">
                                <h1
                                    style={{
                                    color: "red",
                                    marginTop: 5,
                                    fontSize: 13
                                }}>{this.state.testpwd}</h1>
                            </div>

                        </div>
                    </div>

                    <div className="row">
                        <div
                            className="col-md-8 col-12 d-flex align-items-center justify-content-center justify-content-md-start">
                            <a
                                style={{
                                fontSize: 17,
                                color: "#E7B34B"
                            }}
                                onClick={this.openforgetPwd}><FormattedMessage id='forgetyourpassword'/></a>
                        </div>
                        <div
                            className="col-md-4 col-12 d-flex justify-content-center justify-content-md-end">
                            <button
                                type="submit"
                                className="btnGreen"
                                style={{
                                width: 100,
                                marginTop: 20,
                                marginBottom: 20
                            }}
                                onClick={this.login}>
                                <FormattedMessage id='login'/>
                            </button>
                        </div>
                    </div>
                </form>

                <div
                    className="row"
                    style={{
                    marginBottom: 15
                }}>
                    <div className="col-md-12 col-12 d-flex justify-content-center">
                        {/* <h1 className="mySmallTitle text-center">Don\92t have an account ? <button> Sign up now</button></h1> */}
                    </div>
                </div>
            </div>
        } else {
            if (!this.state.checkPin) {
                form = <div key="2">
                    <div className="row">
                        <div
                            className="col-md-12 col-12 d-flex justify-content-end align-items-start"
                            style={{
                            paddingLeft: 0,
                            paddingRight: 0
                        }}>
                            <button onClick={this.props.closeModal} className="btnCloseSmall">X</button>
                        </div>
                    </div>
                    <div
                        className="row"
                        style={{
                        marginBottom: 30,
                        marginTop: 20
                    }}>

                        <div className="col-md-12 col-12 d-flex justify-content-center">
                            <h1 className="myTitle"><FormattedMessage id='ResetPassword'/></h1>
                        </div>

                        <div className="col-md-12 col-12 d-flex justify-content-center">
                            <p className="mb18Regular"><FormattedMessage id='EnterEmailaddress'/></p>
                        </div>

                    </div>

                    <form action="">
                        <div
                            className="row"
                            style={{
                            marginBottom: 30
                        }}>
                            <div className="col-md-12 col-12">
                                <FormattedMessage id='email'>
                                    {placeholder => (<input
                                        type="email"
                                        className="inputSelect myInputText"
                                        style={{
                                        color: 'black',
                                        height: 50,
                                        textAlign: sessionStorage.getItem("locale") === "ar"
                                            ? ('right')
                                            : ('left')
                                    }}
                                        placeholder={placeholder}
                                        value={this.state.forgetPwd_email}
                                        onChange={this.handleChange('forgetPwd_email')}
                                        ref="forgetPwd_email"/>)}
                                </FormattedMessage>
                            </div>
                        </div>

                        <div className="row">
                            <div
                                className="col-md-4 col-12 d-flex align-items-center justify-content-center justify-content-md-start">
                                {/* <a style={{fontSize:17, color:"#85b898"}} onClick={this.openforgetPwd}>Forgot your password ?</a> */}
                            </div>
                            <div
                                className="col-md-8 col-12 d-flex justify-content-center justify-content-md-end">
                                <button
                                    type="submit"
                                    className="btnGreen"
                                    style={{
                                    width: 100,
                                    marginTop: 20,
                                    marginBottom: 20
                                }}
                                    onClick={this._sendEmail}>
                                    <FormattedMessage id='Next'/>
                                </button>
                            </div>
                        </div>
                    </form>

                    <div
                        className="row"
                        style={{
                        marginBottom: 15
                    }}>
                        <div className="col-md-12 col-12 d-flex justify-content-center">
                            {/* <h1 className="mySmallTitle text-center">Don\92t have an account ? <button> Sign up now</button></h1> */}
                        </div>
                    </div>
                </div>
            } else {
                if (!this.state.changePwd) {
                    form = <div key="3">
                        <div className="row">
                            <div
                                className="col-md-12 col-12 d-flex justify-content-end align-items-start"
                                style={{
                                paddingLeft: 0,
                                paddingRight: 0
                            }}>
                                <button onClick={this.props.closeModal} className="btnCloseSmall">X</button>
                            </div>
                        </div>
                        <div
                            className="row"
                            style={{
                            marginBottom: 30,
                            marginTop: 20
                        }}>

                            <div className="col-md-12 col-12 d-flex justify-content-center">
                                <h1 className="myTitle"><FormattedMessage id='ResetPassword'/></h1>
                            </div>

                            <div className="col-md-12 col-12 d-flex justify-content-center">
                                <p className="mb18Regular"><FormattedMessage id='Enterthepin'/></p>
                            </div>

                        </div>

                        <form action="">
                            <div
                                className="row"
                                style={{
                                marginBottom: 30
                            }}>
                                <div className="col-md-12 col-12">
                                    <FormattedMessage id='PIN'>
                                        {placeholder => (<input
                                            type="number"
                                            className="inputSelect myInputText"
                                            style={{
                                            height: 50,
                                            textAlign: sessionStorage.getItem("locale") === "ar"
                                                ? ('right')
                                                : ('left')
                                        }}
                                            placeholder={placeholder}
                                            value={this.state.forgetPwd_pin}
                                            onChange={this.handleChange('forgetPwd_pin')}
                                            ref="forgetPwd_pin"/>)}
                                    </FormattedMessage>
                                </div>
                            </div>

                            <div className="row">
                                <div
                                    className="col-md-4 col-12 d-flex align-items-center justify-content-center justify-content-md-start">
                                    {/* <a style={{fontSize:17, color:"#85b898"}} onClick={this.openforgetPwd}>Forgot your password ?</a> */}
                                </div>
                                <div
                                    className="col-md-8 col-12 d-flex justify-content-center justify-content-md-end">
                                    <button
                                        type="submit"
                                        className="btnGreen"
                                        style={{
                                        width: 100,
                                        marginTop: 20,
                                        marginBottom: 20
                                    }}
                                        onClick={this._checkpin}>
                                        <FormattedMessage id='Next'/>
                                    </button>
                                </div>
                            </div>
                        </form>

                        <div
                            className="row"
                            style={{
                            marginBottom: 15
                        }}>
                            <div className="col-md-12 col-12 d-flex justify-content-center">
                                {/* <h1 className="mySmallTitle text-center">Don\92t have an account ? <button> Sign up now</button></h1> */}
                            </div>
                        </div>
                    </div>
                } else {
                    form = <div key="4">
                        <div className="row">
                            <div
                                className="col-md-12 col-12 d-flex justify-content-end align-items-start"
                                style={{
                                paddingLeft: 0,
                                paddingRight: 0
                            }}>
                                <button onClick={this.props.closeModal} className="btnCloseSmall">X</button>
                            </div>
                        </div>
                        <div
                            className="row"
                            style={{
                            marginBottom: 30,
                            marginTop: 20
                        }}>

                            <div className="col-md-12 col-12 d-flex justify-content-center">
                                <h1 className="myTitle"><FormattedMessage id='ResetPassword'/></h1>
                            </div>

                        </div>

                        <form action="">
                            <div
                                className="row"
                                style={{
                                marginBottom: 30
                            }}>
                                <div className="col-md-12 col-12">
                                    <FormattedMessage id='newpassword'>
                                        {placeholder => (<input
                                            type="password"
                                            className="inputSelect myInputText"
                                            style={{
                                            height: 50,
                                            textAlign: sessionStorage.getItem("locale") === "ar"
                                                ? ('right')
                                                : ('left')
                                        }}
                                            placeholder={placeholder}
                                            value={this.state.forgetPwd_newPwd}
                                            onChange={this.handleChange('forgetPwd_newPwd')}
                                            ref="forgetPwd_newPwd"/>)}
                                    </FormattedMessage>
                                </div>
                            </div>

                            <div
                                className="row"
                                style={{
                                marginBottom: 30
                            }}>
                                <div className="col-md-12 col-12">
                                    <FormattedMessage id='confirmnewpassword'>
                                        {placeholder => (<input
                                            type="password"
                                            className="inputSelect myInputText"
                                            style={{
                                            height: 50,
                                            textAlign: sessionStorage.getItem("locale") === "ar"
                                                ? ('right')
                                                : ('left')
                                        }}
                                            placeholder={placeholder}
                                            value={this.state.forgetPwd_confPwd}
                                            onChange={this.handleChange('forgetPwd_confPwd')}
                                            ref="forgetPwd_confPwd"/>)}
                                    </FormattedMessage>
                                </div>
                            </div>

                            <div className="row">
                                <div
                                    className="col-md-4 col-12 d-flex align-items-center justify-content-center justify-content-md-start">
                                    {/* <a style={{fontSize:17, color:"#85b898"}} onClick={this.openforgetPwd}>Forgot your password ?</a> */}
                                </div>
                                <div
                                    className="col-md-8 col-12 d-flex justify-content-center justify-content-md-end">
                                    <button
                                        type="submit"
                                        className="btnGreen"
                                        style={{
                                        width: 200,
                                        marginTop: 20,
                                        marginBottom: 20
                                    }}
                                        onClick={this._changepwd}>
                                        <FormattedMessage id='changepassword'/>
                                    </button>
                                </div>
                            </div>
                        </form>

                        <div
                            className="row"
                            style={{
                            marginBottom: 15
                        }}>
                            <div className="col-md-12 col-12 d-flex justify-content-center">
                                {/* <h1 className="mySmallTitle text-center">Don\92t have an account ? <button> Sign up now</button></h1> */}
                            </div>
                        </div>
                    </div>
                }
            }
        }
        return (
            <div>
                <Modal
                    isOpen={data.loginPageIsOpen}
                    onRequestClose={this.props.closeModal}
                    style={customStyles}
                    contentLabel="Login">
                    <div className="container" ref="login">

                        {form}

                    </div>

                </Modal>

            </div>
        );
    }
}