/**
 * Auther : Aymen FEZAI
 */

import React, {Component} from 'react';
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import '../../sass/layout/_profilecss.scss'
import '../../images/account.svg';
import '../../images/swiaa_black.png';
import Modal from 'react-modal';
import FontAwesome from 'react-fontawesome'

import {FormattedMessage} from 'react-intl';
//For popup messages
import toastr from 'toastr'
import 'toastr/build/toastr.min.css'

toastr.options = {
    positionClass: 'toast-top-right',
    "closeButton": true,
    hideDuration: 300,
    timeOut: 3000
}
//End of toastr

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        width: '80%',
        maxHeight: "90vh",
        opacity: 1,
        zIndex: 999
    },
    overlay: {
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        zIndex: 999
    }
};

const privacy = {
    maxHeight: "50vh",
    overflow: "scroll"
}

export default class SignupPage extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: '',
            password: '',
            confpassword: '',
            name: '',
            gender: '',
            testpwd: '',
            testname: '',
            testemail: '',
            testgender: '',
            testConfPwd: '',
            type: 'password'
        };

        this.signup = this
            .signup
            .bind(this);
        this.goNext = this
            .goNext
            .bind(this);
        this.goBack = this
            .goBack
            .bind(this);
        this.handleChange = this
            .handleChange
            .bind(this);
        this.showHide = this
            .showHide
            .bind(this);
    }

    /***************show pasword  ***********************************/
    showHide(e) {
        e.preventDefault();
        e.stopPropagation();
        this.setState({
            type: this.state.type === 'input'
                ? 'password'
                : 'input'
        })
    }

    signup(event) {
        event.preventDefault();

        /*let name = this.refs.name.value;
        let email = this.refs.email.value;
        let password = this.refs.password.value;
        let gender = this.refs.gender.value;*/

        let name = this.state.name;
        let email = this.state.email;
        let password = this.state.password;
        let gender = this.state.gender;

        let options = {}

        options.method = 'POST'

        options.headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'x-access-token': 'gd654dg@fuyffd5'
        }

        options.body = JSON.stringify({
            name,
            email,
            password,
            gender,
            role: "client",
            country: "",
            phone: "",
            profilepic: "",
            mailvalidation: false
        })

        fetch("http://127.0.0.1:3000/users/addDummyUser", options)
            .then(res => res.json())
            .then(res => {
                if (res != null) {
                    if (res.message != 'exist') {
                        let options2 = {}

                        options2.method = 'POST'
                        options2.headers = {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        }
                        options2.body = JSON.stringify({email, id: res._id})

                        fetch("http://127.0.0.1:3000/users/sendValidationMail", options2)
                            .then(res => res.json())
                            .then(res => {
                                this
                                    .refs
                                    .creatAccount
                                    .classList
                                    .add('hidden')
                                this
                                    .refs
                                    .privacy
                                    .classList
                                    .add('hidden')
                                this
                                    .refs
                                    .showMessage
                                    .classList
                                    .remove('hidden')
                            })
                            .catch(err => console.error(err))

                    } else {
                        toastr.error("Email exist ! Please verify your credentials and try again");
                    }
                }
            })
            .catch(err => console.error(err))
    }

    goNext(event) {
        event.preventDefault();

        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        /* let name = this.refs.name.value;
        let email = this.refs.email.value;
        let password = this.refs.password.value;
        let confpassword = this.refs.confpassword.value;
        let gender = this.refs.gender.value;*/

        let name = this.state.name;
        let email = this.state.email;
        let password = this.state.password;
        let confpassword = this.state.confpassword;
        let gender = this.state.gender;

        if (password == "") {
            document
                .getElementById('password')
                .style
                .borderColor = "red";
            this.setState({testpwd: "Password field shouldn't be empty"});
        } else if (password.length < 6) {
            document
                .getElementById('password')
                .style
                .borderColor = "red";
            this.setState({testpwd: "password must be greater than 6"});
        } else {
            document
                .getElementById('password')
                .style
                .borderColor = "green";
            this.setState({testpwd: ""});
        }

        if (name == "") {
            document
                .getElementById('name')
                .style
                .borderColor = "red";
            this.setState({testname: "Full name field shouldn't be empty"});
        } else {
            document
                .getElementById('name')
                .style
                .borderColor = "green";
            this.setState({testname: ""});
        }

        if (email == "") {
            document
                .getElementById('email')
                .style
                .borderColor = "red";
            this.setState({testemail: "Email field shouldn't be empty"});
        } else if (!email.match(mailformat)) {
            document
                .getElementById('email')
                .style
                .borderColor = "red";
            this.setState({testemail: "This doesn’t look like an email address.."});
        } else {
            document
                .getElementById('email')
                .style
                .borderColor = "green";
            this.setState({testemail: ""});
        }

        if ((confpassword == "" || confpassword != password)) {
            document
                .getElementById('confpassword')
                .style
                .borderColor = "red";
            this.setState({testConfPwd: "Password field shouldn't be empty and must be identical"});
        } else {
            document
                .getElementById('confpassword')
                .style
                .borderColor = "green";
            this.setState({testConfPwd: ""});
        }

        if (gender == "") {
            document
                .getElementById('gender')
                .style
                .borderColor = "red";
            this.setState({testgender: "gender field shouldn't be empty"});
        } else {
            document
                .getElementById('gender')
                .style
                .borderColor = "green";
            this.setState({testgender: ""});
        }
        let options = {}

        options.method = 'POST'

        options.headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'x-access-token': 'gd654dg@fuyffd5'
        }

        options.body = JSON.stringify({email})

        if (email == "") {
            // toastr.info("Email field shouldn't be empty");
            this
                .refs
                .email
                .classList
                .add('invalidInput')
        } else {
            if (!email.match(mailformat)) {
                // toastr.info("This doesn’t look  like an email address..")
                this
                    .refs
                    .email
                    .classList
                    .add('invalidInput')
            } else {
                if (name == "") {
                    // toastr.info("Name field shouldn't be empty")
                    this
                        .refs
                        .name
                        .classList
                        .add('invalidInput')
                } else {
                    if (gender == "") {
                        // toastr.info("Please choose your gender")
                        this
                            .refs
                            .gender
                            .classList
                            .add('invalidInput')
                    } else {
                        if (password == "" || password != confpassword) {
                            // toastr.info("Password should mach")
                            this
                                .refs
                                .password
                                .classList
                                .add('invalidInput')
                            this
                                .refs
                                .confpassword
                                .classList
                                .add('invalidInput')
                        } else {
                            fetch("http://127.0.0.1:3000/users/emailExistence", options)
                                .then(res => res.json())
                                .then(res => {
                                    console.log(res)
                                    if (res != null) {
                                        if (res.message == 'exist') {
                                            toastr.error("Email exist ! Please verify your credentials and try again");
                                            this
                                                .refs
                                                .email
                                                .classList
                                                .add('invalidInput')
                                        } else {
                                            this
                                                .refs
                                                .creatAccount
                                                .classList
                                                .add('hidden')
                                            this
                                                .refs
                                                .privacy
                                                .classList
                                                .remove('hidden')
                                            this
                                                .refs
                                                .showMessage
                                                .classList
                                                .add('hidden')
                                        }
                                    }
                                })
                                .catch(err => console.error(err))
                        }
                    }
                }
            }
        }

    }

    goBack(event) {
        event.preventDefault();
        this
            .refs
            .creatAccount
            .classList
            .remove('hidden');
        this
            .refs
            .showMessage
            .classList
            .add('hidden');
        this
            .refs
            .privacy
            .classList
            .add('hidden');
    }

    handleChange(e) {
        e
            .target
            .classList
            .remove('invalidInput')
        toastr.info(e.target.value);
    }

    /*********************** get value from input ****************************/
    handleChange = name => event => {
        this.setState({[name]: event.target.value});
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        switch (name) {
            case "name":
                if (event.target.value == "") {
                    document
                        .getElementById('name')
                        .style
                        .borderColor = "red";
                    this.setState({testname: "Full name field shouldn't be empty"});
                } else {
                    document
                        .getElementById('name')
                        .style
                        .borderColor = "green";
                    this.setState({testname: ""});
                }
                break;
            case "email":

                if (event.target.value == "") {
                    document
                        .getElementById('email')
                        .style
                        .borderColor = "red";
                    this.setState({testemail: "Email field shouldn't be empty"});
                } else if (!(event.target.value).match(mailformat)) {
                    document
                        .getElementById('email')
                        .style
                        .borderColor = "red";
                    this.setState({testemail: "This doesn’t look like an email address.."});
                } else {
                    document
                        .getElementById('email')
                        .style
                        .borderColor = "green";
                    this.setState({testemail: ""});
                }
                break;
            case "password":
                if (event.target.value == "") {
                    document
                        .getElementById('password')
                        .style
                        .borderColor = "red";
                    this.setState({testpwd: "Password                            field shouldn't be empty"});
                } else if (event.target.value.length < 6) {
                    document
                        .getElementById('password')
                        .style
                        .borderColor = "red";
                    this.setState({testpwd: "password must be greater than 6"});
                } else {
                    document
                        .getElementById('password')
                        .style
                        .borderColor = "green";
                    this.setState({testpwd: ""});
                }
                break;

            case "confpassword":

                if ((event.target.value == "" || event.target.value != this.state.password)) {
                    document
                        .getElementById('confpassword')
                        .style
                        .borderColor = "red";
                    this.setState({testConfPwd: "Password field shouldn't be empty and must be identical"});
                } else {
                    document
                        .getElementById('confpassword')
                        .style
                        .borderColor = "green";
                    this.setState({testConfPwd: ""});
                }
                break;

            case "gender":
                if (event.target.value == "") {
                    document
                        .getElementById('gender')
                        .style
                        .borderColor = "red";
                    this.setState({testgender: "Gender field shouldn't be empty"});
                } else {
                    document
                        .getElementById('gender')
                        .style
                        .borderColor = "green";
                    this.setState({testgender: ""});
                }
                break;
        }
    }
    /*************************************************************************/

    render() {
        var data = this.props.data;

        return (
            <div>

                <Modal
                    isOpen={data.signupPageIsOpen}
                    onRequestClose={this.props.closeModal}
                    style={customStyles}
                    contentLabel="Signup">
                    <div
                        className="container"
                        style={{
                        direction: sessionStorage.getItem("locale") === "ar"
                            ? "rtl"
                            : "ltr"
                    }}>

                        <div className="row">
                            <div
                                className="col-md-12 d-flex justify-content-end align-items-start"
                                style={{
                                paddingLeft: 0,
                                paddingRight: 0
                            }}>
                                <button onClick={this.props.closeModal} className="btnCloseSmall">X</button>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-8 d-flex align-items-center">

                                <div className="container" ref="creatAccount">

                                    <div
                                        className="col-md-12"
                                        style={{
                                        marginBottom: 20
                                    }}>
                                        {/* <img src="../../images/Logo.svg" style={{}}/> */}
                                        <div className="row">
                                            <h1 className="myTitle"><FormattedMessage id='CreateyourSwiaaAccount'/></h1>
                                        </div>
                                        <div className="row">
                                            <p className="mb18Regular"><FormattedMessage id='JoinSwiaaforfree'/></p>
                                        </div>
                                    </div>

                                    <form action="">

                                        <div
                                            className="row"
                                            style={{
                                            marginBottom: 20
                                        }}>
                                            <div className="col-md-12 col-12">
                                                <FormattedMessage id='email'>
                                                    {placeholder => (<input
                                                        type="email"
                                                        id="email"
                                                        className=" col-md-12 col-12 inputSelect myInputText"
                                                        style={{
                                                        color: 'balck',
                                                        height: 50,
                                                        textAlign: sessionStorage.getItem("locale") === "ar"
                                                            ? ('right')
                                                            : ('left')
                                                    }}
                                                        placeholder={placeholder}
                                                        value={this.state.email}
                                                        onChange={this.handleChange('email')}
                                                        ref="email"
                                                        autoFocus="true"/>)}
                                                </FormattedMessage>
                                                <div className="col-md-12 col-12">
                                                    <h1
                                                        style={{
                                                        color: "red",
                                                        marginTop: 5
                                                    }}>{this.state.testemail}</h1>
                                                </div>

                                            </div>
                                        </div>

                                        <div
                                            className="row"
                                            style={{
                                            marginBottom: 20
                                        }}>
                                            <div className="col-md-6 col-12">
                                                <FormattedMessage id='fullname'>
                                                    {placeholder => (<input
                                                        type="text"
                                                        id="name"
                                                        className="inputSelect myInputText"
                                                        style={{
                                                        color: 'black',
                                                        height: 50,
                                                        textAlign: sessionStorage.getItem("locale") === "ar"
                                                            ? ('right')
                                                            : ('left')
                                                    }}
                                                        placeholder={placeholder}
                                                        value={this.state.name}
                                                        onChange={this.handleChange('name')}
                                                        ref="name"/>)}
                                                </FormattedMessage>

                                            </div>

                                            <div className="col-md-6 col-12">
                                                <select
                                                    name="Gender"
                                                    id="gender"
                                                    style={{
                                                    height: 50,
                                                    paddingTop: 15
                                                }}
                                                    className="inputSelect myInputText"
                                                    value={this.state.gender}
                                                    onChange={this.handleChange('gender')}
                                                    ref="gender">
                                                    <FormattedMessage id='gender'>
                                                        {(id) => <option value="value1">{id}</option>}
                                                    </FormattedMessage>
                                                    <FormattedMessage id="male">
                                                        {(id) => <option value="value2">{id}</option>}
                                                    </FormattedMessage>
                                                    <FormattedMessage id="female">
                                                        {(id) => <option value="value3">{id}</option>}
                                                    </FormattedMessage>
                                                </select>

                                            </div>

                                            <div className="col-md-6 col-12">
                                                <h1
                                                    style={{
                                                    color: "red",
                                                    marginTop: 5
                                                }}>{this.state.testname}</h1>
                                            </div>
                                            <div className="col-md-6 col-12">
                                                <h1
                                                    style={{
                                                    color: "red",
                                                    marginTop: 5
                                                }}>{this.state.testgender}</h1>
                                            </div>

                                        </div>

                                        <div
                                            className="row"
                                            style={{
                                            marginBottom: 20
                                        }}>
                                            <div className="col-md-6 col-12">
                                                <FormattedMessage id='password'>
                                                    {placeholder => (<input
                                                        id="password"
                                                        className="inputSelect myInputText"
                                                        style={{
                                                        color: 'black',
                                                        height: 50,
                                                        textAlign: sessionStorage.getItem("locale") === "ar"
                                                            ? ('right')
                                                            : ('left')
                                                    }}
                                                        placeholder={placeholder}
                                                        value={this.state.password}
                                                        onChange={this.handleChange('password')}
                                                        ref="password"
                                                        type={this.state.type}/>)}
                                                </FormattedMessage>

                                                <span
                                                    className="password__show"
                                                    onClick={this.showHide}
                                                    style={{
                                                    marginTop: 15
                                                }}>{this.state.type === 'input'
                                                        ? <img
                                                                src="../images/eye_close.png"
                                                                style={{
                                                                width: 20
                                                            }}/>
                                                        : <img
                                                            src="../images/eye_open.png"
                                                            style={{
                                                            width: 20
                                                        }}/>}</span>
                                            </div>
                                            <div className="col-md-6 col-12">
                                                <FormattedMessage id='confpassword'>
                                                    {placeholder => (<input
                                                        id="confpassword"
                                                        className="inputSelect myInputText"
                                                        style={{
                                                        color: 'black',
                                                        height: 50,
                                                        textAlign: sessionStorage.getItem("locale") === "ar"
                                                            ? ('right')
                                                            : ('left')
                                                    }}
                                                        placeholder={placeholder}
                                                        value={this.state.confpassword}
                                                        onChange={this.handleChange('confpassword')}
                                                        ref="confpassword"
                                                        type={this.state.type}/>)}
                                                </FormattedMessage>

                                                <span
                                                    className="password__show"
                                                    onClick={this.showHide}
                                                    style={{
                                                    marginTop: 15
                                                }}>{this.state.type === 'input'
                                                        ? <img
                                                                src="../images/eye_close.png"
                                                                style={{
                                                                width: 20
                                                            }}/>
                                                        : <img
                                                            src="../images/eye_open.png"
                                                            style={{
                                                            width: 20
                                                        }}/>}</span>
                                            </div>
                                            <div className="col-md-6 col-12">
                                                <h1
                                                    style={{
                                                    color: "red",
                                                    marginTop: 5
                                                }}>{this.state.testpwd}</h1>
                                            </div>
                                            <div className="col-md-6 col-12">
                                                <h1
                                                    style={{
                                                    color: "red",
                                                    marginTop: 5
                                                }}>{this.state.testConfPwd}</h1>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div
                                                className="col-md-8 col-12 d-flex align-items-center justify-content-center justify-content-md-start">
                                                {/* <h1 className="mySmallTitle text-center">Sign in instead</h1> */}
                                            </div>

                                            <div
                                                className="col-md-4 col-12 d-flex justify-content-center justify-content-md-end">
                                                <button
                                                    type="submit"
                                                    className="btnGreen"
                                                    style={{
                                                    width: 100,
                                                    marginTop: 20,
                                                    marginBottom: 20
                                                }}
                                                    onClick={this.goNext}>
                                                    <FormattedMessage id='Next'/>
                                                </button>
                                            </div>
                                        </div>

                                    </form>
                                </div>

                                {/* Privacy */}
                                <div
                                    className="container hidden"
                                    ref="privacy"
                                    style={{
                                    marginLeft: 20
                                }}>
                                    <div className="row">
                                        <h1
                                            className="myTitle"
                                            style={{
                                            marginBottom: 20
                                        }}><FormattedMessage id='privacyandTerms'/></h1>
                                    </div>

                                    <div className="row" style={privacy} id="box">
                                        <p
                                            style={{
                                            marginBottom: 10
                                        }}>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eleifend, nunc
                                            sit amet volutpat porta, felis purus feugiat dolor, vel auctor nisl lectus
                                            elementum risus. Quisque orci erat, tristique ut eleifend ac, vehicula sit amet
                                            leo. Nunc non dui tortor. Sed venenatis, sem non mollis suscipit, odio sem
                                            consectetur leo, sit amet commodo nibh mi vitae metus. Lorem ipsum dolor sit
                                            amet, consectetur adipiscing elit. Suspendisse quis nisi sit amet risus placerat
                                            volutpat. Fusce placerat enim eu arcu ullamcorper, quis commodo arcu tempor.
                                            Maecenas ut luctus risus. Pellentesque lacinia fringilla metus ac facilisis.
                                            Fusce porta mauris quam, quis finibus neque bibendum non. Nunc id justo et orci
                                            sodales tincidunt. Vestibulum rutrum dui id tempor tristique. Morbi dapibus
                                            commodo felis, at hendrerit est congue id. Cras fringilla non urna eget posuere.
                                        </p>
                                        <p
                                            style={{
                                            marginBottom: 10
                                        }}>
                                            Nam tempus erat ut velit faucibus, id maximus sem mollis. Morbi nec nibh a nisl
                                            ultrices posuere ut et purus. Curabitur molestie magna consectetur, venenatis
                                            metus id, placerat nunc. Proin ut luctus lorem. Nullam pharetra ac ante at
                                            volutpat. Donec eu tellus fringilla, egestas libero posuere, consequat lacus.
                                            Morbi nisi ex, mollis at dictum id, lacinia ac erat. Cras vitae pellentesque
                                            nisl, auctor ullamcorper sem. Aliquam erat volutpat. Interdum et malesuada fames
                                            ac ante ipsum primis in faucibus. Nullam vel leo egestas, tristique nibh at,
                                            gravida massa. Aenean at tristique lacus. Sed elit quam, sagittis ac tempus sit
                                            amet, egestas ut dolor. Praesent pulvinar, purus at congue tempor, nisi eros
                                            finibus felis, a ornare elit massa vel nunc. Vivamus non tempor ex.
                                        </p>
                                        <p
                                            style={{
                                            marginBottom: 10
                                        }}>
                                            Vivamus eros metus, congue ultricies felis sed, porttitor lacinia mi. Vestibulum
                                            ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nam
                                            posuere bibendum tortor, et ultricies mi fringilla ac. Ut sed libero nibh. Ut
                                            nec volutpat tortor, eget aliquet tortor. Proin porta tortor quis massa
                                            porttitor, et blandit enim rutrum. Sed efficitur turpis id metus consectetur
                                            elementum. In fringilla nunc quis magna sodales ullamcorper. Ut eu mi blandit,
                                            interdum sem in, vestibulum nisi. Nunc feugiat mauris ut justo tempus, sed
                                            fringilla nisl fringilla. Aenean cursus ipsum malesuada, aliquet nibh sit amet,
                                            venenatis ipsum.
                                        </p>
                                    </div>

                                    <div className="row">
                                        <div
                                            className="col-md-6 col-12 d-flex justify-content-center justify-content-md-start"
                                            style={{
                                            paddingLeft: 0
                                        }}>
                                            <button
                                                type="submit"
                                                className="btnWhite"
                                                style={{
                                                width: 100,
                                                marginTop: 20,
                                                marginBottom: 20
                                            }}
                                                onClick={this.goBack}>
                                                <FormattedMessage id='back'/>
                                            </button>
                                        </div>
                                        <div
                                            className="col-md-6 col-12 d-flex justify-content-center justify-content-md-end"
                                            style={{
                                            paddingRight: 0
                                        }}>
                                            <button
                                                type="submit"
                                                className="btnGreen"
                                                style={{
                                                width: 100,
                                                marginTop: 20,
                                                marginBottom: 20
                                            }}
                                                onClick={this.signup}>
                                                <FormattedMessage id='IAGREE'/>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                {/* Show check you mail message */}
                                <div className="container hidden" ref="showMessage">
                                    <div className="row">
                                        <h1
                                            className="myTitle"
                                            style={{
                                            marginTop: 20,
                                            marginBottom: 20
                                        }}><FormattedMessage id='WelcometoSwiaa'/></h1>
                                    </div>
                                    <div
                                        className="row"
                                        style={{
                                        marginLeft: 40
                                    }}>
                                        <h3
                                            className="mySmallTitle"
                                            style={{
                                            marginBottom: 10
                                        }}><FormattedMessage id='Onesteptogo'/></h3>
                                    </div>
                                    <div
                                        className="row"
                                        style={{
                                        marginLeft: 40
                                    }}>
                                        <h3 className="mySmallTitle"><FormattedMessage id='Pleasecheckyouremail'/></h3>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 d-flex justify-content-center align-items-center">
                                <img
                                    src="../../images/swiaa_black.png"
                                    style={{
                                    height: 244
                                }}/>
                            </div>
                        </div>

                    </div>
                </Modal>
            </div>

        );
    }
}
